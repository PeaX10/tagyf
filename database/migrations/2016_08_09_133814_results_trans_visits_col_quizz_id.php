<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResultsTransVisitsColQuizzId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('results', function(Blueprint $table){
            $table->integer('quizz_id')->unsigned()->after('tag_id');
        });
        Schema::table('visits', function(Blueprint $table){
            $table->integer('quizz_id')->unsigned()->after('tag_id');
        });
        Schema::table('translations', function(Blueprint $table){
            $table->integer('quizz_id')->unsigned()->after('tag_id');
        });
        Schema::table('answers', function(Blueprint $table){
            $table->integer('quizz_id')->unsigned()->after('tag_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('results', function(Blueprint $table){
            $table->dropColumn('quizz_id');
        });
        Schema::table('visits', function(Blueprint $table){
            $table->dropColumn('quizz_id');
        });
        Schema::table('translations', function(Blueprint $table){
            $table->dropColumn('quizz_id');
        });
        Schema::table('answers', function(Blueprint $table){
            $table->dropColumn('quizz_id');
        });
    }
}

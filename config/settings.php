<?php

return [
    'fonts' => [
        public_path('fonts/roboto/Roboto-Black.ttf') => 'Roboto Black',
        public_path('fonts/roboto/Roboto-BlackItalic.ttf') => 'Roboto Black Italic',
        public_path('fonts/roboto/Roboto-Bold.ttf') => 'Roboto Bold',
        public_path('fonts/roboto/Roboto-Italic.ttf') => 'Roboto Italic',
        public_path('fonts/roboto/Roboto-Light.ttf') => 'Roboto Light',
        public_path('fonts/roboto/Roboto-LightItalic.ttf') => 'Roboto Light Italic',
        public_path('fonts/roboto/Roboto-Medium.ttf') => 'Roboto Medium',
        public_path('fonts/roboto/Roboto-MediumItalic.ttf') => 'Roboto Medium Italic',
        public_path('fonts/roboto/Roboto-Regular.ttf') => 'Roboto Regular',
        public_path('fonts/roboto/Roboto-Thin.ttf') => 'Roboto Thin',
        public_path('fonts/roboto/Roboto-ThinItalic.ttf') => 'Roboto Thin Italic',
        public_path('fonts/roboto/RobotoCondensed-Bold.ttf') => 'Roboto Condensed Bold',
        public_path('fonts/roboto/RobotoCondensed-BoldItalic.ttf') => 'Roboto Condensed Bold Italic',
        public_path('fonts/roboto/RobotoCondensed-Italic.ttf') => 'Roboto Condensed Italic',
        public_path('fonts/roboto/RobotoCondensed-Light.ttf') => 'Roboto Condensed Light',
        public_path('fonts/roboto/RobotoCondensed-LightItalic.ttf') => 'Roboto Condensed Light Italic',
        public_path('fonts/roboto/RobotoCondensed-Regular.ttf') => 'Roboto Condensed Regular',
        public_path('fonts/pacifico/Pacifico.ttf') => 'Pacifico',
        public_path('fonts/allura/Allura-Regular.otf') => 'Allura Regular',
        public_path('fonts/amatic/AmaticSC-Regular.ttf') => 'Amatic Regular',
        public_path('fonts/amatic/Amatic-Bold.ttf') => 'Amatic Bold',
        public_path('fonts/ChunkFive/Chunkfive.otf') => 'ChunkFive',
        public_path('fonts/GoodDog/GoodDog.otf') => 'GoodDog',
        public_path('fonts/great-vibes/GreatVibes-Regular.otf') => 'GreatVibes Regular',
        public_path('fonts/Lobster_1.3/Lobster.otf') => 'Lobster',
        public_path('fonts/kaushan-script/KaushanScript-Regular.tg') => 'KaushanScript',
        public_path('fonts/dafont/FunSized.ttf') => 'FunSized',
        public_path('fonts/dafont/MonaShark.otf') => 'Mona Shark',
        public_path('fonts/dafont/SpaceComics.ttf') => 'Space Comics',
        public_path('fonts/dafont/LemonMilk.otf') => 'Lemon Milk',
    ],
    'modules' => [
        'fb_me',
        'fb_friends',
        'carbon',
    ],
    'langs' => [
        'fr' => 'Français',
        'en' => 'English',
    ],
    'soon_langs' => [
        'it' => 'Italian',
    ],
    'fake_profil' => [
        0 => [
            'id' => 1,
            'name' => 'Mathieu Dupond',
            'gender' => 'male',
            'email' => 'mathieu.dupond@gmail.com',
            'avatar' => 'http://cdn1-elle.ladmedia.fr/var/plain_site/storage/images/beaute/news-beaute/parfums/on-a-rencontre-vinnie-woolston-le-beau-gosse-de-la-nuit-de-l-homme-2980853/56038205-1-fre-FR/On-a-rencontre-Vinnie-Woolston-le-beau-gosse-de-La-Nuit-de-L-Homme.jpg',
            'firstname' => 'Mathieu',
            'name' => 'Dupond'
        ]
    ]
];
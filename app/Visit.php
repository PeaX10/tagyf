<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $table = 'visits';

    protected $fillable = ['id', 'tag_id', 'user_id', 'lang', 'image', 'ip', 'previous_url'];

    public function tag(){
        return $this->belongsTo('App\Tag');
    }

    public function quizz(){
        return $this->belongsTo('App\Quizz');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}

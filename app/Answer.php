<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answers';

    protected $fillable = ['id', 'tag_id', 'background', 'gender', 'builder', 'visible'];

    public function tag(){
        return $this->belongsTo('App\Tag');
    }
}

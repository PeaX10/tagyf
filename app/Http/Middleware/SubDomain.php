<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class SubDomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $pieces = explode('.', $request->getHost());
        $subdomain = $pieces[0];

        if($subdomain == env('APP_ADMIN_SUBDOMAIN')){
            //(null !== $request->cookie('lang')) ? App::setLocale($request->cookie('lang')) : App::setLocale('en');
            App::setLocale('fr');
        }else if(array_key_exists($subdomain, config('settings.langs'))){

            App::setLocale($subdomain);
        }else{
            (null !== $request->cookie('lang')) ? App::setLocale($request->cookie('lang')) : App::setLocale('fr');
            return redirect('https://fr.'.env('APP_DOMAIN'));
        }

        return $next($request);
    }
}

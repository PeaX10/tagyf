<?php

namespace App\Http\Controllers\Admin;

use App\Translation;
use Illuminate\Http\Request;
use App\Quizz;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class QuizzController extends Controller
{
    public function index(){
        $quizzs = Quizz::orderBy('id', 'desc')->with('translations')->get();
        return view('back.quizz.index', compact('quizzs'));
    }

    public function destroy(Request $request, $id){
        if($request->ajax()){
            $quizz = Quizz::findOrFail($id);
            unlink(public_path('uploads/quizz/'.$quizz->image));
            unlink(public_path('uploads/quizz/'.$quizz->playImage));
            $quizz->translations()->delete();
            $quizz->visits()->delete();
            foreach($quizz->results()->get() as $key => $value){
                if(file_exists(public_path('uploads/result/'.$value->image))) unlink(public_path('uploads/result/'.$value->image));
            }
            $quizz->results()->delete();
            foreach($quizz->answers()->get() as $key => $value){
                if(file_exists(public_path('uploads/answer/'.$value->background))) unlink(public_path('uploads/answer/'.$value->background));
                if(file_exists(public_path('uploads/answer/preview/'.$value->id.'.jpg'))) unlink(public_path('uploads/answer/preview/'.$value->id.'.jpg'));
                $builder = json_decode($value->builder);
                $composants = $builder->composants;
                foreach($composants as $key2 => $value2){
                    if(!empty($value2->image) && file_exists(public_path('uploads/composant/'.$value2->image))){
                        unlink(public_path('uploads/composant/'.$value2->image));
                    }
                    if(!empty($value2->custom_mask) && file_exists(public_path('uploads/composant/'.$value2->custom_mask))){
                        unlink(public_path('uploads/composant/'.$value2->custom_mask));
                    }
                }
            }
            $quizz->answers()->delete();
            $quizz->delete();
            return ['success' => true];
        }else{
            return abort(404);
        }
    }

    public function create(){
        return view('back.quizz.create');
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:5',
            'playDescription' => 'required|min:10',
            'resultDescription' => 'required|min:10',
            'gender' => 'required|in:all,male,female',
            'type' => 'required|in:friend,random',
            'image' => 'required|image|max:8192|dimensions:width=1200,height=630',
            'playImage' => 'required|image|max:8192|dimensions:width=1200,height=630'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            // On encode notre slug et on regarde si il existe ou non en BDD
            $slug = str_slug(str_random(rand(1,5)).time().str_random(rand(2, 6)));
            $ext = Input::file('image')->getClientOriginalExtension();
            $image_name =  'q_'.time().str_random(rand(15, 20)).'.'.$ext;
            if($ext != 'gif'){
                $image = Image::make(Input::file('image'))->save(public_path('uploads/quizz/'.$image_name));
            }else{
                Input::file('image')->move(public_path('uploads/quizz'), $image_name);
            }
            $ext = Input::file('playImage')->getClientOriginalExtension();
            $play_image_name =  'q_'.time().str_random(rand(15, 20)).'.'.$ext;
            if($ext != 'gif'){
                $playImage = Image::make(Input::file('playImage'))->save(public_path('uploads/quizz/'.$play_image_name));
            }else{
                Input::file('playImage')->move(public_path('uploads/quizz'), $image_name);
            }
            $quizz = new Quizz();
            $quizz->slug = $slug;
            if(Input::get('visible')) $quizz->visible = 1;
            $quizz->image = $image_name;
            $quizz->playImage = $play_image_name;
            $quizz->gender = Input::get('gender');
            $quizz->langs = implode(',', Input::get('langs'));
            $quizz->type = Input::get('type');
            $quizz->save();

            foreach(Input::get('langs') as $key => $value){
                $lang = new Translation();
                $lang->quizz_id = $quizz->id;
                $lang->lang = $value;
                $data = array('title' => Input::get('name'), 'playDescription' => Input::get('playDescription'), 'resultDescription' => Input::get('resultDescription'));
                $lang->data = json_encode($data);
                $lang->save();
            }

            return redirect('quizz')->with('added_quizz', true);
        }

    }

    public function edit($id){
        $quizz = quizz::findOrFail($id);
        $langs = explode(',', $quizz->langs);
        $visits = $quizz->visits()->orderBy('created_at', 'desc')->get();
        $results = $quizz->results()->orderBy('created_at', 'desc')->get();
        $answers = $quizz->answers()->orderBy('created_at', 'desc')->get();
        return view('back.quizz.edit', compact('quizz', 'langs', 'visits', 'results', 'answers'));
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'slug' => 'required|unique:quizzs,slug,'.$id.'|min:5',
            'gender' => 'required|in:all,male,female',
            'type' => 'required|in:friend,random',
            'image' => 'image|max:8192|dimensions:min_width=1200,min_height=630',
            'playImage' => 'image|max:8192|dimensions:min_width=1200,min_height=630'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            // On encode notre slug et on regarde si il existe ou non en BDD
            $slug = str_slug(Input::get('slug'));
            if(quizz::where('slug', $slug)->whereNotIn('id', [$id])->exists()) {
                $validator->errors()->add('slug', 'Ce slug existe déjà !');
                return back()->withErrors($validator)->withInput();
            }else{
                $quizz = Quizz::findOrFail($id);
                if($request->hasFile('image')){
                    if(file_exists(public_path('uploads/quizz/'.$quizz->image))) unlink(public_path('uploads/quizz/'.$quizz->image));
                    $ext = Input::file('image')->getClientOriginalExtension();
                    $image_name =  'q_'.time().str_random(rand(15, 20)).'.'.$ext;
                    if($ext != 'gif'){
                        $image = Image::make(Input::file('image'))->save(public_path('uploads/quizz/'.$image_name));
                    }else{
                        Input::file('image')->move(public_path('uploads/quizz'), $image_name);
                    }
                    $quizz->image = $image_name;
                }
                if($request->hasFile('playImage')){
                    if(file_exists(public_path('uploads/quizz/'.$quizz->playImage))) unlink(public_path('uploads/quizz/'.$quizz->playImage));
                    $ext = Input::file('playImage')->getClientOriginalExtension();
                    $image_name =  'q_'.time().str_random(rand(15, 20)).'.'.$ext;
                    if($ext != 'gif'){
                        $image = Image::make(Input::file('playImage'))->save(public_path('uploads/quizz/'.$image_name));
                    }else{
                        Input::file('playImage')->move(public_path('uploads/quizz'), $image_name);
                    }
                    $quizz->playImage = $image_name;
                }
                $quizz->slug = $slug;
                if(Input::get('visible')) $quizz->visible = 1; else $quizz->visible = 0;
                $quizz->gender = Input::get('gender');
                $quizz->type = Input::get('type');
                $quizz->save();

                return back()->with('edited_quizz', true);
            }
        }
    }
}

<?php

namespace App\Http\Controllers\Admin\Quizz;

use App\Quizz;
use App\Translation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class LangController extends Controller
{
    /**
     * @param Request $request
     * @param $quizz_id
     * @param $lang
     * @return array
     */
    public function destroy(Request $request, $quizz_id, $lang){
        if($request->ajax()){
            $quizz = Quizz::findOrFail($quizz_id);
            $quizz->translations()->where('lang', $lang)->firstOrFail()->delete();
            $langs = explode(',', $quizz->langs);
            foreach($langs as $key => $value){
                if($value == $lang || $value == '') unset($langs[$key]);
            }
            $quizz->langs = implode(',', $langs);
            $quizz->save();
            return ['success' => true];
        }else{
            return abort(404);
        }
    }

    public function show($quizz_id, $lang){
        $trans = Translation::where('quizz_id', $quizz_id)->where('lang', $lang)->firstOrFail();
        return redirect('translation/'.$trans->id.'/edit');
    }

    public function edit($quizz_id, $lang){
        $lang = Translation::where('lang', $lang)->where('quizz_id', $quizz_id)->firstOrFail();
        if($lang->visible){
            $lang->visible = 0;
        }else{
            $lang->visible = 1;
        }
        $lang->save();
        return back()->with('lang_added', $lang->lang);
    }

    public function store(Request $request, $quizz_id){

        if($request->ajax() && !Quizz::where('id', $quizz_id)->where('langs', 'LIKE', '%'.Input::get('lang').'%')->exists()){
            $lang = new Translation();
            $lang->lang = Input::get('lang');
            $lang->quizz_id = Input::get('quizz_id');
            $data = array('title' => 'NaN');
            $lang->data = json_encode($data);
            $lang->save();

            $quizz = quizz::findOrFail($quizz_id);
            $langs = explode(',', $quizz->langs);
            array_push($langs, Input::get('lang'));
            $quizz->langs = implode(',', $langs);
            $quizz->save();

            return json_encode($lang);
        }else{
            return abort(404);
        }
    }
}

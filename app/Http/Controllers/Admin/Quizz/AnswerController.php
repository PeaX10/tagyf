<?php

namespace App\Http\Controllers\Admin\Quizz;

use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Answer;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;

class AnswerController extends Controller
{
    public function create($quizz_id){
        return view('back.quizz.answer.create', compact('quizz_id'));
    }

    public function store(Request $request, $quizz_id){
        $validator = Validator::make($request->all(), [
            'nb_text' => 'numeric|min:0|max:10',
            'nb_img' => 'numeric|min:0|max:10',
            'gender' => 'required|in:all,male,female',
            'image' => 'required|image|max:4096|dimensions:width=1200,height=630'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $image_name =  'a_'.time().str_random(rand(15, 20)).'.png';
            $image = Image::make(Input::file('image'))->save(public_path('uploads/answer/'.$image_name));
            $answer = new Answer();
            $answer->quizz_id = $quizz_id;
            if(Input::get('visible')) $answer->visible = 1;
            $answer->background = $image_name;
            $answer->gender = Input::get('gender');
            $builder = array();
            for ($i = 0; $i < Input::get('nb_text'); $i++) {
                $builder[$i] = ['id' => $i, 'name' => 'text_'.$i, 'type' => 'text', 'pos_x' => 0, 'pos_y' => 0, 'width' => 100, 'height' => 16, 'angle' => 0, 'index' => $i, 'b_o_f' => 'front', 'font_size' => 40, 'font_color' => '#FFFFFF', 'font_family' => public_path('fonts/dafont/SpaceComics.ttf'), 'vertical_align' => 'middle', 'horizontal_align' => 'center', 'stroke_size' => 2, 'stroke_color' => '#000000', 'object' => 'Test text '.$i];
            }

            for ($i = Input::get('nb_text'); $i < Input::get('nb_text') + Input::get('nb_img'); $i++) {
                $builder[$i] = ['id' => $i, 'name' => 'img_'.$i, 'type' => 'picture', 'pos_x' => 50, 'pos_y' => 50, 'width' => 25, 'height' => 25, 'angle' => 0, 'index' => $i, 'b_o_f' => 'front', 'mask' => 'regular', 'opacity' => "100", 'blur' => 0, 'brightness' => 0, 'contrast' => 0, 'object' => '{! me.avatar !}'];
            }
            $answer->builder = json_encode(array('composants' => $builder));
            $answer->save();

            return redirect('quizz/'.$quizz_id.'/edit')->with('added_answer', true);

        }
    }

    public function destroy(Request $request, $quizz_id, $answer_id){
        if($request->ajax()){
            $answer = Answer::findOrFail($answer_id);
            if(file_exists(public_path('uploads/answer/'.$answer->background))) unlink(public_path('uploads/answer/'.$answer->background));
            if(file_exists(public_path('uploads/answer/preview/'.$answer->id.'.jpg'))) unlink(public_path('uploads/answer/preview/'.$answer->id.'.jpg'));
            // On souhaite également supprimé les images qu'il pourrait y avoir dans les composants d'une application
            $builder = json_decode($answer->builder);
            $composants = $builder->composants;
            foreach($composants as $key => $value){
                if(!empty($value->image) && file_exists(public_path('uploads/composant/'.$value->image))){
                    unlink(public_path('uploads/composant/'.$value->image));
                }
                if(!empty($value->custom_mask) && file_exists(public_path('uploads/composant/'.$value->custom_mask))){
                    unlink(public_path('uploads/composant/'.$value->custom_mask));
                }
            }
            $answer->delete();
            return ['success' => true];
        }else{
            return abort(404);
        }
    }

    public function edit($quizz_id, $answer_id){
        $answer = Answer::findOrFail($answer_id);
        return view('back.quizz.answer.edit', compact('answer'));
    }

    public function update(Request $request, $quizz_id, $answer_id){
        $validator = Validator::make($request->all(), [
            'gender' => 'required|in:all,male,female',
            'image' => 'image|max:8096|dimensions:width=1200,height=630'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            // On encode notre slug et on regarde si il existe ou non en BDD
            $answer = Answer::findOrFail($answer_id);
            if($request->hasFile('image')){
                unlink(public_path('uploads/answer/'.$answer->background));
                $image_name =  'a_'.time().str_random(rand(15, 20)).'.png';
                $image = Image::make(Input::file('image'))->save(public_path('uploads/answer/'.$image_name));
                $answer->background = $image_name;
            }
            if(Input::get('visible')) $answer->visible = 1; else $answer->visible = 0;
            $answer->gender = Input::get('gender');
            $answer->save();

            return redirect('quizz/'.$quizz_id.'/edit')->with('edited_answer', true);
        }
    }

    public function show($quizz_id, $answer_id, LaravelFacebookSdk $fb){
        $answer = Answer::findOrFail($answer_id);
        $builder = json_decode($answer->builder);
        $composants = $builder->composants;
        $this->previewAnswer($answer_id, $fb);
        return view('back.quizz.answer.show', compact('answer', 'composants', 'builder'));
    }

    public function updatePos(Request $request, $quizz_id, $answer_id){
        if($request->ajax()){
            $answer = Answer::findOrFail($answer_id);
            $builder_db = json_decode($answer->builder);
            $builder = $builder_db->composants;
            $data = Input::get('pos');
            foreach($data as $key => $value){
                $builder[$key]->pos_x = $value['left'];
                $builder[$key]->pos_y = $value['top'];
                $builder[$key]->width = $value['width'];
                $builder[$key]->height = $value['height'];
                $builder[$key]->angle = $value['angle'];
                $builder[$key]->index = $value['index'];
            }
            $builder_db->composants = $builder;
            $answer->builder = json_encode($builder_db);
            $answer->save();
            return ['success' => true];
        }
    }

    public function previewAnswer($answer_id, LaravelFacebookSdk $fb){
        $answer = Answer::findOrFail($answer_id);
        $builder = json_decode($answer->builder);
        $composants = $builder->composants;
        // On trie les composants par index
        usort($composants, function($a, $b) {
            return $a->index <=> $b->index;
        });
        $img = Image::canvas(1200, 630);
        if(count((array) $composants) > 0){
            foreach($composants as $key => $value) {
                $value->width = round(($value->width / 100) * 1200, 0);
                $value->pos_x = round(($value->pos_x / 100) * 1200, 0);
                $value->height = round(($value->height / 100) * 630, 0);
                $value->pos_y = round(($value->pos_y / 100) * 630, 0);
            }
            // On regarde les images qui sont en dessous
            foreach($composants as $key => $value){
                // Si l'image se trouve en dessous
                if($value->b_o_f == "back"){
                    if($value->type == 'picture'){
                        $img->insert($this->buildPic($value, $fb), 'top-left', $value->pos_x, $value->pos_y);
                    }else{
                        $img->insert($this->buildText($value, $fb), 'top-left', $value->pos_x, $value->pos_y);
                    }
                }
            }
            // On met l'image de fond
            $img->insert(Image::make(public_path('uploads/answer/'.$answer->background)));
            // On regarde les images qui sont au dessus
            foreach($composants as $key => $value){
                // Si l'image se trouve en dessous
                if($value->b_o_f == "front"){
                    if($value->type == 'picture'){
                        $img->insert($this->buildPic($value, $fb), 'top-left', $value->pos_x, $value->pos_y);
                    }else{
                        $img->insert($this->buildText($value, $fb), 'top-left', $value->pos_x, $value->pos_y);
                    }
                }
            }
            // On insert le watermark
            $img->insert(Image::make(public_path('img/watermark.png')), 'top-left', 3, 3);
        }else{
            $img->insert(Image::make(public_path('uploads/answer/'.$answer->background)));
        }


        // On supprime l'ancienne image si elle existe
        if (File::exists(public_path('uploads/answer/preview/'.$answer_id.'.jpg'))){
            unlink(public_path('uploads/answer/preview/'.$answer_id.'.jpg'));
        }

        $img->save(public_path('uploads/answer/preview/'.$answer_id.'.jpg'));
    }

    public function buildPic($composant, LaravelFacebookSdk $fb){
        $user = Auth::user();
        if(!empty($composant->image)){
            $temp = Image::make('uploads/composant/'.$composant->image);
        }else{
            if($composant->object == '{! friend1.avatar !}'){
                // On doit récupèrer l'image voulu depuis FB
                $friends = $this->getFriends($fb);
                $friend = $friends[array_rand($friends, 1)];
                $temp = Image::make($friend['picture']);
            }else if($composant->object == '{! me.avatar !}'){
                $temp = Image::make(public_path('uploads/avatar/'.$user->avatar));
            }else{
                $temp = Image::canvas($composant->width, $composant->height);
            }
        }
        $temp->resize($composant->width, $composant->height);

        // Mask
        if($composant->mask == 'circle'){
            $mask = Image::canvas($composant->width, $composant->height);
            $mask->circle($composant->width, $composant->width/2, $composant->height/2, function ($draw) {
                $draw->background('#fff');
            });
            $temp->mask($mask, false);
        }else if($composant->mask == 'custom'){
            $mask = Image::make(public_path('uploads/composant/'.$composant->custom_mask));
            $temp->mask($mask, false);
        }
        // Opacity
        if($composant->opacity != "100"){
            $temp->opacity($composant->opacity);
        }
        // Blur
        if($composant->blur != "0"){
            $temp->blur($composant->blur);
        }
        // Brightness
            if($composant->brightness != "0"){
                $temp->brightness($composant->brightness);
        }
        // Contrast
        if($composant->contrast != "0"){
            $temp->contrast($composant->contrast);
        }
        // Rotation
        if($composant->angle != "0"){
            $temp->rotate($composant->angle);
        }
        return $temp;
    }

    public function buildText($composant, LaravelFacebookSdk $fb){
        $text = $this->completeText($composant->object, $fb);
        $temp = Image::canvas($composant->width, $composant->height);
        if($composant->horizontal_align == 'left'){
            $x = 0;
            $y = ($composant->height)/2;
        }else if($composant->horizontal_align == 'right'){
            $x = $composant->width;
            $y = ($composant->height)/2;
        }else{
            $x = ($composant->width)/2;
            $y = ($composant->height)/2;
        }
        $temp->text($text, $x, $y, function($font) use ($composant){
            if($composant->font_family){
                $font->file($composant->font_family);
            }else{
                $font->file(public_path('fonts/roboto/Roboto-Regular.ttf'));
            }
            $font->size($composant->font_size);
            $font->color($composant->font_color);
            $font->align($composant->horizontal_align);
            $font->valign($composant->vertical_align);
            $font->strokeWidth($composant->stroke_size);
            $font->strokeColor($composant->stroke_color);
            $font->angle($composant->angle);
        });
        return $temp;
    }

    public function getFriends(LaravelFacebookSdk $fb){
        $token = Auth::user()->fb_token;
        $fb->setDefaultAccessToken($token);
        try {
            $response = $fb->get('/me/taggable_friends?fields=id,name,picture.type(square).width(1200).height(1200),gender,first_name,last_name');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            return back()->with('error_fb', true);
        }
        $friends = array();
        $end = true;
        $fb_friends = $response->getGraphEdge();
        foreach($fb_friends as $friend) {
            $data = [
                'id' => $friend->getField('id'),
                'name' => $friend->getField('name'),
                'first_name' => $friend->getField('first_name'),
                'last_name' => $friend->getField('last_name'),
                'gender' => $friend->getField('gender'),
                'picture' => $friend->getField('picture')->getField('url')
            ];
            array_push($friends, $data);
        }
        return $friends;
    }

    public function completeText($text, LaravelFacebookSdk $fb){
        $user = Auth::user();
        $friends = $this->getFriends($fb);
        $friend = $friends[array_rand($friends, 1)];
        $ar = [
            '{! me.name !}' => $this->transliterateString($user->name),
            '{! me.first_name !}' => $this->transliterateString($user->first_name),
            '{! me.last_name !}' => $this->transliterateString($user->last_name),
            '{! friend1.name !}' => $this->transliterateString($friend['name']),
            '{! friend1.first_name !}' => $this->transliterateString($friend['first_name']),
            '{! friend1.last_name !}' => $this->transliterateString($friend['last_name']),
            '{! ME.NAME !}' => strtoupper($this->transliterateString($user->name)),
            '{! ME.FIRST_NAME !}' => strtoupper($this->transliterateString($user->first_name)),
            '{! ME.LAST_NAME !}' => strtoupper($this->transliterateString($user->last_name)),
            '{! FRIEND1.NAME !}' => strtoupper($this->transliterateString($friend['name'])),
            '{! FRIEND1.FIRST_NAME !}' => strtoupper($this->transliterateString($friend['first_name'])),
            '{! FRIEND1.LAST_NAME !}' => strtoupper($this->transliterateString($friend['last_name'])),
        ];
        return str_replace(array_keys($ar), array_values($ar), $text);
    }

    function transliterateString($txt) {
        $transliterationTable = array('á' => 'a', 'Á' => 'A', 'à' => 'a', 'À' => 'A', 'ă' => 'a', 'Ă' => 'A', 'â' => 'a', 'Â' => 'A', 'å' => 'a', 'Å' => 'A', 'ã' => 'a', 'Ã' => 'A', 'ą' => 'a', 'Ą' => 'A', 'ā' => 'a', 'Ā' => 'A', 'ä' => 'ae', 'Ä' => 'AE', 'æ' => 'ae', 'Æ' => 'AE', 'ḃ' => 'b', 'Ḃ' => 'B', 'ć' => 'c', 'Ć' => 'C', 'ĉ' => 'c', 'Ĉ' => 'C', 'č' => 'c', 'Č' => 'C', 'ċ' => 'c', 'Ċ' => 'C', 'ç' => 'c', 'Ç' => 'C', 'ď' => 'd', 'Ď' => 'D', 'ḋ' => 'd', 'Ḋ' => 'D', 'đ' => 'd', 'Đ' => 'D', 'ð' => 'dh', 'Ð' => 'Dh', 'é' => 'e', 'É' => 'E', 'è' => 'e', 'È' => 'E', 'ĕ' => 'e', 'Ĕ' => 'E', 'ê' => 'e', 'Ê' => 'E', 'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'ė' => 'e', 'Ė' => 'E', 'ę' => 'e', 'Ę' => 'E', 'ē' => 'e', 'Ē' => 'E', 'ḟ' => 'f', 'Ḟ' => 'F', 'ƒ' => 'f', 'Ƒ' => 'F', 'ğ' => 'g', 'Ğ' => 'G', 'ĝ' => 'g', 'Ĝ' => 'G', 'ġ' => 'g', 'Ġ' => 'G', 'ģ' => 'g', 'Ģ' => 'G', 'ĥ' => 'h', 'Ĥ' => 'H', 'ħ' => 'h', 'Ħ' => 'H', 'í' => 'i', 'Í' => 'I', 'ì' => 'i', 'Ì' => 'I', 'î' => 'i', 'Î' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ĩ' => 'i', 'Ĩ' => 'I', 'į' => 'i', 'Į' => 'I', 'ī' => 'i', 'Ī' => 'I', 'ĵ' => 'j', 'Ĵ' => 'J', 'ķ' => 'k', 'Ķ' => 'K', 'ĺ' => 'l', 'Ĺ' => 'L', 'ľ' => 'l', 'Ľ' => 'L', 'ļ' => 'l', 'Ļ' => 'L', 'ł' => 'l', 'Ł' => 'L', 'ṁ' => 'm', 'Ṁ' => 'M', 'ń' => 'n', 'Ń' => 'N', 'ň' => 'n', 'Ň' => 'N', 'ñ' => 'n', 'Ñ' => 'N', 'ņ' => 'n', 'Ņ' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ò' => 'o', 'Ò' => 'O', 'ô' => 'o', 'Ô' => 'O', 'ő' => 'o', 'Ő' => 'O', 'õ' => 'o', 'Õ' => 'O', 'ø' => 'oe', 'Ø' => 'OE', 'ō' => 'o', 'Ō' => 'O', 'ơ' => 'o', 'Ơ' => 'O', 'ö' => 'oe', 'Ö' => 'OE', 'ṗ' => 'p', 'Ṗ' => 'P', 'ŕ' => 'r', 'Ŕ' => 'R', 'ř' => 'r', 'Ř' => 'R', 'ŗ' => 'r', 'Ŗ' => 'R', 'ś' => 's', 'Ś' => 'S', 'ŝ' => 's', 'Ŝ' => 'S', 'š' => 's', 'Š' => 'S', 'ṡ' => 's', 'Ṡ' => 'S', 'ş' => 's', 'Ş' => 'S', 'ș' => 's', 'Ș' => 'S', 'ß' => 'SS', 'ť' => 't', 'Ť' => 'T', 'ṫ' => 't', 'Ṫ' => 'T', 'ţ' => 't', 'Ţ' => 'T', 'ț' => 't', 'Ț' => 'T', 'ŧ' => 't', 'Ŧ' => 'T', 'ú' => 'u', 'Ú' => 'U', 'ù' => 'u', 'Ù' => 'U', 'ŭ' => 'u', 'Ŭ' => 'U', 'û' => 'u', 'Û' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ű' => 'u', 'Ű' => 'U', 'ũ' => 'u', 'Ũ' => 'U', 'ų' => 'u', 'Ų' => 'U', 'ū' => 'u', 'Ū' => 'U', 'ư' => 'u', 'Ư' => 'U', 'ü' => 'ue', 'Ü' => 'UE', 'ẃ' => 'w', 'Ẃ' => 'W', 'ẁ' => 'w', 'Ẁ' => 'W', 'ŵ' => 'w', 'Ŵ' => 'W', 'ẅ' => 'w', 'Ẅ' => 'W', 'ý' => 'y', 'Ý' => 'Y', 'ỳ' => 'y', 'Ỳ' => 'Y', 'ŷ' => 'y', 'Ŷ' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y', 'ź' => 'z', 'Ź' => 'Z', 'ž' => 'z', 'Ž' => 'Z', 'ż' => 'z', 'Ż' => 'Z', 'þ' => 'th', 'Þ' => 'Th', 'µ' => 'u', 'а' => 'a', 'А' => 'a', 'б' => 'b', 'Б' => 'b', 'в' => 'v', 'В' => 'v', 'г' => 'g', 'Г' => 'g', 'д' => 'd', 'Д' => 'd', 'е' => 'e', 'Е' => 'E', 'ё' => 'e', 'Ё' => 'E', 'ж' => 'zh', 'Ж' => 'zh', 'з' => 'z', 'З' => 'z', 'и' => 'i', 'И' => 'i', 'й' => 'j', 'Й' => 'j', 'к' => 'k', 'К' => 'k', 'л' => 'l', 'Л' => 'l', 'м' => 'm', 'М' => 'm', 'н' => 'n', 'Н' => 'n', 'о' => 'o', 'О' => 'o', 'п' => 'p', 'П' => 'p', 'р' => 'r', 'Р' => 'r', 'с' => 's', 'С' => 's', 'т' => 't', 'Т' => 't', 'у' => 'u', 'У' => 'u', 'ф' => 'f', 'Ф' => 'f', 'х' => 'h', 'Х' => 'h', 'ц' => 'c', 'Ц' => 'c', 'ч' => 'ch', 'Ч' => 'ch', 'ш' => 'sh', 'Ш' => 'sh', 'щ' => 'sch', 'Щ' => 'sch', 'ъ' => '', 'Ъ' => '', 'ы' => 'y', 'Ы' => 'y', 'ь' => '', 'Ь' => '', 'э' => 'e', 'Э' => 'e', 'ю' => 'ju', 'Ю' => 'ju', 'я' => 'ja', 'Я' => 'ja');
        return str_replace(array_keys($transliterationTable), array_values($transliterationTable), $txt);
    }

    public function cloneAnswer(Request $request, $quizz_id, $answer_id){
        $answer = Answer::findOrFail($answer_id)->replicate();
        // On souhaite également copié les images qu'il pourrait y avoir dans les composants d'une application
        $builder = json_decode($answer->builder);
        $composants = $builder->composants;
        foreach($composants as $key => $value){
            if(!empty($value->image) && file_exists(public_path('uploads/composant/'.$value->image))){
                $image_name = 'image_'.time().str_random(30).'.png';
                Image::make(public_path('uploads/composant/'.$value->image))->save(public_path('uploads/composant/'.$image_name));
                $composants[$key]->image = $image_name;
            }
            if(!empty($value->custom_mask) && file_exists(public_path('uploads/composant/'.$value->custom_mask))){
                $mask_name = 'mask_'.time().str_random(30).'.png';
                Image::make(public_path('uploads/composant/'.$value->custom_mask))->save(public_path('uploads/composant/'.$mask_name));
                $composants[$key]->custom_mask = $mask_name;
            }
        }
        $builder->composants = $composants;
        $answer->builder = json_encode($builder);
        $image_name =  'a_'.time().str_random(rand(15, 20)).'.png';
        Image::make(public_path('uploads/answer/'.$answer->background))->save(public_path('uploads/answer/'.$image_name));
        $answer->background = $image_name;
        $answer->save();
        return back();
    }
}

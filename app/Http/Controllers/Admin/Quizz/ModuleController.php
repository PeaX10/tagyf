<?php

namespace App\Http\Controllers\Admin\Quizz;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ModuleController extends Controller
{
    public function create($tag_id, $answer_id){
        return view('back.quizz.answer.module.create');
    }
}

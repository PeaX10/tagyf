<?php

namespace App\Http\Controllers\Admin;

use App\Answer;
use App\Quizz;
use App\Tag;
use App\Translation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class TranslationController extends Controller
{
    /**
     * @param Request $request
     * @param $quizz_id
     * @param $lang
     * @return array
     */
    public function index(){
        $translations = Translation::all();
        return view('back.translation.index', compact('translations'));
    }

    public function destroy(Request $request, $id){
        if($request->ajax()){
            $trans = Translation::findOrFail($id);
            if($trans->quizz_id > 0){
                $type = Quizz::findOrFail($trans->quizz_id);
            }else if($trans->tag_id > 0){
                $type = Tag::findOrFail($trans->tag_id);
            }else{
                abord(404);
            }
            $lang = $trans->lang;
            $langs = explode(',', $type->langs);
            foreach($langs as $key => $value){
                if($value == $lang || $value == '') unset($langs[$key]);
            }
            $type->langs = implode(',', $langs);
            $type->save();
            $trans->delete();
            return ['success' => true];
        }else{
            return abort(404);
        }
    }

    public function edit($id){
        $trans = Translation::findOrFail($id);
        if($trans->quizz_id > 0){
            $type = Quizz::findOrFail($trans->quizz_id);
        }else if($trans->tag_id > 0){
            $type = Tag::findOrFail($trans->tag_id);
        }else{
            abord(404);
        }

        $translations = json_decode($trans->data);
        $answers = $type->answers()->get();
        foreach($answers as $answer){
            $composants = json_decode($answer->builder)->composants;
            foreach($composants as $composant){
                $slug = 'answer'.$answer->id.'_composant'.$composant->id;
                if(!empty($composant->object) && empty($translations->$slug)) $translations->$slug = $composant->object;
            }
        }
        return view('back.translation.edit', compact('trans', 'translations'));
    }

    public function update(Request $request, $id){
        $trans = Translation::findOrFail($id);
        $translations = $request->all();
        unset($translations['_token']);
        unset($translations['_method']);
        $data = json_encode($translations);
        $trans->data = $data;
        $trans->save();
        return redirect('translation')->with('translation_updated', true);
    }
}

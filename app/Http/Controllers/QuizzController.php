<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Quizz;
use App\Result;
use Illuminate\Support\Facades\App;
use App\Visit;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;

class QuizzController extends Controller
{
    public function index(Request $request, $slug, LaravelFacebookSdk $fb){
        $pagequizz = Quizz::join('translations', 'translations.quizz_id', '=', 'quizzs.id')
            ->where('quizzs.langs', 'LIKE', '%'.App::getLocale().'%')
            ->where('translations.lang', App::getLocale())
            ->where('translations.visible', true)
            ->where('quizzs.visible', true)
            ->where('quizzs.slug', $slug)
            ->orderBy('quizzs.created_at', 'desc')
            ->select('translations.*', 'quizzs.*')
            ->with('translations')
            ->firstOrFail();

        $quizzs = Quizz::join('translations', 'translations.quizz_id', '=', 'quizzs.id')
            ->where('quizzs.langs', 'LIKE', '%'.App::getLocale().'%')
            ->where('translations.lang', App::getLocale())
            ->where('translations.visible', true)
            ->where('quizzs.visible', true)
            ->orderBy('quizzs.created_at', 'desc')
            ->select('translations.*', 'quizzs.*')
            ->with('translations')
            ->simplePaginate(18);
        if(Auth::check()) {
            $user = Auth::user();
            $friends = json_decode($user->friends);
        }

        $this->updateVisit($request, $pagequizz->id);
        return view('front.quizz.show', compact('pagequizz', 'quizzs', 'friends'));
    }
    

    public function result($slug){
        $result = Result::where('slug', $slug)->firstOrFail();
        $quizzs = Quizz::join('translations', 'translations.quizz_id', '=', 'quizzs.id')
            ->where('quizzs.langs', 'LIKE', '%'.App::getLocale().'%')
            ->where('translations.lang', App::getLocale())
            ->where('translations.visible', true)
            ->where('quizzs.visible', true)
            ->orderBy('quizzs.created_at', 'desc')
            ->select('translations.*', 'quizzs.*')
            ->with('translations')
            ->simplePaginate(18);

        return view('front.quizz.result', compact('result', 'quizzs'));
    }

    public function updateVisit(Request $request, $quizz_id){
        // On vérifie si il n'y a pas eu de visite par le même utilisateur dans les 2 dernières minutes
        $nb_visits = Visit::where('ip', $request->ip())->where('quizz_id', $quizz_id)->where('created_at', '>=', Carbon::now()->subMinute(2))->count();
        if($nb_visits === 0){
            $visit = new Visit();
            if(Auth::check()) $visit->user_id = Auth::user()->id; else $visit->user_id = 0;
            $visit->quizz_id = $quizz_id;
            $visit->lang = App::getLocale();
            $visit->ip = $request->ip();
            $visit->previous_url = redirect()->back()->getTargetUrl();
            $visit->save();
        }
    }

    public function doQuizz(Request $request, $slug, LaravelFacebookSdk $fb){
        if(!$this->checkPermissions($fb)) return redirect($fb->getReRequestUrl(['email', 'user_friends']));
        if($request->has('friend')){
            $friend = $this->getFriend(Input::get('friend'), $fb);
        }
        else{
            $friend = $this->randomFriend($fb);
        }
        $answer = quizz::where('slug', $slug)->firstOrFail()->answers()->where('visible', true)->get()->random(1);
        $result = $this->makeResult($answer, $friend);
        return redirect('qr/'.$result->slug);
    }

    public function getFriend($fb_name, LaravelFacebookSdk $fb){
        $token = Auth::user()->fb_token;
        $fb->setDefaultAccessToken($token);
        Session::put('fb_user_access_token', (string) $token);

        try {
            $response = $fb->get('/me/taggable_friends?fields=id,name,picture.type(square).width(1200).height(1200),gender,first_name,last_name');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            Auth::logout();
            return back()->with('error_fb', true);
        }
        $end = true;
        $fb_friends = $response->getGraphEdge();
        while($end){
            foreach($fb_friends as $dfriend) {
                if($dfriend->getField('name') == $fb_name){
                    $friend = [
                        'id' => $dfriend->getField('id'),
                        'name' => $dfriend->getField('name'),
                        'first_name' => $dfriend->getField('first_name'),
                        'last_name' => $dfriend->getField('last_name'),
                        'gender' => $dfriend->getField('gender'),
                        'picture' => $dfriend->getField('picture')->getField('url')
                    ];
                    $end = false;
                }
            }
            if(isset($fb_friends->getMetaData('paging')['paging']['next'])){
                $after = $fb_friends->getMetaData('paging')['paging']['cursors']['after'];
                try {
                    $response = $fb->get('/me/taggable_friends?fields=id,name,picture.type(square).width(1200).height(1200),first_name,last_name,gender&after='.$after);
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    return back()->with('error_fb', true);
                }
                $fb_friends = $response->getGraphEdge();
            }else{
                $end = false;
            }
        }
        if(empty($friend)){
            return back()->with('error_fb');
        }else{
            return $friend;
        }
    }

    public function randomFriend(LaravelFacebookSdk $fb){
        $token = Auth::user()->fb_token;
        $fb->setDefaultAccessToken($token);
        Session::put('fb_user_access_token', (string) $token);

        try {
            $response = $fb->get('/me/taggable_friends?fields=id,name,first_name,last_name,gender,picture.type(square).width(1200).height(1200)&limit=100');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            return back()->with('error_fb', true);
        }
        $fb_friends = $response->getGraphEdge();
        $dfriend = $fb_friends[rand(0, count($fb_friends) - 1)];
        $friend = [
            'id' => $dfriend->getField('id'),
            'name' => $dfriend->getField('name'),
            'first_name' => $dfriend->getField('first_name'),
            'last_name' => $dfriend->getField('last_name'),
            'gender' => $dfriend->getField('gender'),
            'picture' => $dfriend->getField('picture')->getField('url')
        ];

        if(empty($friend)){
            return back()->with('error_fb');
        }else{
            return $friend;
        }
    }

    public function completeText($text, $friend, $fake=null){
        if(Auth::check()) $user = Auth::user();
        if($fake || !Auth::check()){
            $user = collect();
            $user->put('name', 'Mon nom');
            $user->put('first_name', 'Mon prénom');
            $user->put('last_name', 'Mon nom');

        }else{
            $user['name'] = $user->name;
            $user['first_name'] = $user->first_name;
            $user['last_name'] = $user->last_name;
        }

        $ar = [
            '{! me.name !}' => $this->transliterateString($user['name']),
            '{! me.first_name !}' => $this->transliterateString($user['first_name']),
            '{! me.last_name !}' => $this->transliterateString($user['last_name']),
            '{! friend1.name !}' => $this->transliterateString($friend['name']),
            '{! friend1.first_name !}' => $this->transliterateString($friend['first_name']),
            '{! friend1.last_name !}' => $this->transliterateString($friend['last_name']),
            '{! ME.NAME !}' => strtoupper($this->transliterateString($user['name'])),
            '{! ME.FIRST_NAME !}' => strtoupper($this->transliterateString($user['first_name'])),
            '{! ME.LAST_NAME !}' => strtoupper($this->transliterateString($user['last_name'])),
            '{! FRIEND1.NAME !}' => strtoupper($this->transliterateString($friend['name'])),
            '{! FRIEND1.FIRST_NAME !}' => strtoupper($this->transliterateString($friend['first_name'])),
            '{! FRIEND1.LAST_NAME !}' => strtoupper($this->transliterateString($friend['last_name'])),
        ];
        return str_replace(array_keys($ar), array_values($ar), $text);
    }

    public function buildPic($composant, $friend, $fake=null){
        $user = Auth::user();
        if(!empty($composant->image)){
            $temp = Image::make('uploads/composant/'.$composant->image);
        }else{
            if($composant->object == '{! friend1.avatar !}'){
                if($fake) $temp = $friend['picture'] = public_path('img/placeholder.jpg');
                $temp = Image::make($friend['picture']);
            }else if($composant->object == '{! me.avatar !}'){
                if($fake) $temp = $user->avatar = public_path('img/placeholder.jpg');
                $temp = Image::make(public_path('uploads/avatar/'.$user->avatar));
            }else{
                $temp = Image::canvas($composant->width, $composant->height);
            }
        }
        $temp->resize($composant->width, $composant->height);

        // Mask
        if($composant->mask == 'circle'){
            $mask = Image::canvas($composant->width, $composant->height);
            $mask->circle($composant->width, $composant->width/2, $composant->height/2, function ($draw) {
                $draw->background('#fff');
            });
            $temp->mask($mask, false);
        }else if($composant->mask == 'custom'){
            $mask = Image::make(public_path('uploads/composant/'.$composant->custom_mask));
            $temp->mask($mask, false);
        }
        // Opacity
        if($composant->opacity != "100"){
            $temp->opacity($composant->opacity);
        }
        // Blur
        if($composant->blur != "0"){
            $temp->blur($composant->blur);
        }
        // Brightness
        if($composant->brightness != "0"){
            $temp->brightness($composant->brightness);
        }
        // Contrast
        if($composant->contrast != "0"){
            $temp->contrast($composant->contrast);
        }
        // Rotation
        if($composant->angle != "0"){
            $temp->rotate($composant->angle);
        }
        return $temp;
    }

    public function buildText($composant, $friend, $fake=null){
        if(empty($fake)) $fake = null;
        $text = $this->completeText($composant->object, $friend, $fake);
        $temp = Image::canvas($composant->width, $composant->height);
        if($composant->horizontal_align == 'left'){
            $x = 0;
            $y = ($composant->height)/2;
        }else if($composant->horizontal_align == 'right'){
            $x = $composant->width;
            $y = ($composant->height)/2;
        }else{
            $x = ($composant->width)/2;
            $y = ($composant->height)/2;
        }
        $temp->text($text, $x, $y, function($font) use ($composant){
            $font->file($composant->font_family);
            $font->size($composant->font_size);
            $font->color($composant->font_color);
            $font->align($composant->horizontal_align);
            $font->valign($composant->vertical_align);
            $font->strokeWidth($composant->stroke_size);
            $font->strokeColor($composant->stroke_color);
            $font->angle($composant->angle);
        });
        return $temp;
    }

    public function makeResult($answer, $friend, $name=null){
        if(empty($name)) $name = null;
        $builder = json_decode($answer->builder);
        $composants = $builder->composants;
        // On trie les composants par index
        usort($composants, function($a, $b) {
            return $a->index <=> $b->index;
        });
        $img = Image::canvas(1200, 630);
        foreach($composants as $key => $value) {
            $value->width = round(($value->width / 100) * 1200, 0);
            $value->pos_x = round(($value->pos_x / 100) * 1200, 0);
            $value->height = round(($value->height / 100) * 630, 0);
            $value->pos_y = round(($value->pos_y / 100) * 630, 0);
        }
        // On regarde les images qui sont en dessous
        foreach($composants as $key => $value){
            // Si l'image se trouve en dessous
            if($value->b_o_f == "back"){
                if($value->type == 'picture'){
                    $img->insert($this->buildPic($value, $friend, $name), 'top-left', $value->pos_x, $value->pos_y);
                }else{
                    $img->insert($this->buildText($value, $friend, $name), 'top-left', $value->pos_x, $value->pos_y);
                }
            }
        }
        // On met l'image de fond
        $img->insert(Image::make(public_path('uploads/answer/'.$answer->background)));
        // On regarde les images qui sont au dessus
        foreach($composants as $key => $value){
            // Si l'image se trouve en dessous
            if($value->b_o_f == "front"){
                if($value->type == 'picture'){
                    $img->insert($this->buildPic($value, $friend, $name), 'top-left', $value->pos_x, $value->pos_y);
                }else{
                    $img->insert($this->buildText($value, $friend, $name), 'top-left', $value->pos_x, $value->pos_y);
                }
            }
        }
        // On insert le watermark
        $img->insert(Image::make(public_path('img/watermark.png')), 'top-left', 3, 3);
        // On supprime l'ancienne image si elle existe
        if(empty($name)){
            $slug = str_random(rand(2,6)).time().str_random(rand(3,9));
            $result_name = 'r_'.time().str_random(20).'.jpg';
        }else{
            $slug = 'answer_g_'.$name;
            $result_name = $slug.'.jpg';
        }
        $img->save(public_path('uploads/result/'.$result_name));
        $result = new Result();
        $result->quizz_id = $answer->quizz_id;
        if(Auth::check()) $result->user_id = Auth::user()->id;
        $result->slug = $slug;
        $result->image = $result_name;
        $result->lang = App::getLocale();
        $result->ip = $this->get_client_ip();
        $result->save();
        return $result;
    }

    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public function newToken(LaravelFacebookSdk $fb){
        if(Auth::check())  $user = Auth::user(); else return redirect('/auth/facebook');
        $token = $user->fb_token;
        if(!$token) return redirect('/auth/facebook');
        $oauth_client = $fb->getOAuth2Client();
        try {
            $token = $oauth_client->getLongLivedAccessToken($token);
        }catch(Facebook\Exceptions\FacebookSDKException $e) {
            Auth::logout();
            return redirect('/auth/facebook')->with('alert_error');
        }

        $user->fb_token = (string) $token;
        $user->save();

        return $token;
    }

    public function checkPermissions($fb){
        $token = $this->newToken($fb);
        $fb->setDefaultAccessToken($token);
        Session::put('fb_user_access_token', (string) $token);
        try {
            $response = $fb->get('/me/permissions');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            Auth::logout();
            return back()->with('error_fb', true);
        }
        $permissions = $response->getGraphEdge();
        $autorization = true;
        foreach($permissions as $permission){
            if($permission['status'] == 'declined') $autorization = false;
        }
        return $autorization;
    }

    function transliterateString($txt){
        $transliterationTable = array('á' => 'a', 'Á' => 'A', 'à' => 'a', 'À' => 'A', 'ă' => 'a', 'Ă' => 'A', 'â' => 'a', 'Â' => 'A', 'å' => 'a', 'Å' => 'A', 'ã' => 'a', 'Ã' => 'A', 'ą' => 'a', 'Ą' => 'A', 'ā' => 'a', 'Ā' => 'A', 'ä' => 'ae', 'Ä' => 'AE', 'æ' => 'ae', 'Æ' => 'AE', 'ḃ' => 'b', 'Ḃ' => 'B', 'ć' => 'c', 'Ć' => 'C', 'ĉ' => 'c', 'Ĉ' => 'C', 'č' => 'c', 'Č' => 'C', 'ċ' => 'c', 'Ċ' => 'C', 'ç' => 'c', 'Ç' => 'C', 'ď' => 'd', 'Ď' => 'D', 'ḋ' => 'd', 'Ḋ' => 'D', 'đ' => 'd', 'Đ' => 'D', 'ð' => 'dh', 'Ð' => 'Dh', 'é' => 'e', 'É' => 'E', 'è' => 'e', 'È' => 'E', 'ĕ' => 'e', 'Ĕ' => 'E', 'ê' => 'e', 'Ê' => 'E', 'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'ė' => 'e', 'Ė' => 'E', 'ę' => 'e', 'Ę' => 'E', 'ē' => 'e', 'Ē' => 'E', 'ḟ' => 'f', 'Ḟ' => 'F', 'ƒ' => 'f', 'Ƒ' => 'F', 'ğ' => 'g', 'Ğ' => 'G', 'ĝ' => 'g', 'Ĝ' => 'G', 'ġ' => 'g', 'Ġ' => 'G', 'ģ' => 'g', 'Ģ' => 'G', 'ĥ' => 'h', 'Ĥ' => 'H', 'ħ' => 'h', 'Ħ' => 'H', 'í' => 'i', 'Í' => 'I', 'ì' => 'i', 'Ì' => 'I', 'î' => 'i', 'Î' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ĩ' => 'i', 'Ĩ' => 'I', 'į' => 'i', 'Į' => 'I', 'ī' => 'i', 'Ī' => 'I', 'ĵ' => 'j', 'Ĵ' => 'J', 'ķ' => 'k', 'Ķ' => 'K', 'ĺ' => 'l', 'Ĺ' => 'L', 'ľ' => 'l', 'Ľ' => 'L', 'ļ' => 'l', 'Ļ' => 'L', 'ł' => 'l', 'Ł' => 'L', 'ṁ' => 'm', 'Ṁ' => 'M', 'ń' => 'n', 'Ń' => 'N', 'ň' => 'n', 'Ň' => 'N', 'ñ' => 'n', 'Ñ' => 'N', 'ņ' => 'n', 'Ņ' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ò' => 'o', 'Ò' => 'O', 'ô' => 'o', 'Ô' => 'O', 'ő' => 'o', 'Ő' => 'O', 'õ' => 'o', 'Õ' => 'O', 'ø' => 'oe', 'Ø' => 'OE', 'ō' => 'o', 'Ō' => 'O', 'ơ' => 'o', 'Ơ' => 'O', 'ö' => 'oe', 'Ö' => 'OE', 'ṗ' => 'p', 'Ṗ' => 'P', 'ŕ' => 'r', 'Ŕ' => 'R', 'ř' => 'r', 'Ř' => 'R', 'ŗ' => 'r', 'Ŗ' => 'R', 'ś' => 's', 'Ś' => 'S', 'ŝ' => 's', 'Ŝ' => 'S', 'š' => 's', 'Š' => 'S', 'ṡ' => 's', 'Ṡ' => 'S', 'ş' => 's', 'Ş' => 'S', 'ș' => 's', 'Ș' => 'S', 'ß' => 'SS', 'ť' => 't', 'Ť' => 'T', 'ṫ' => 't', 'Ṫ' => 'T', 'ţ' => 't', 'Ţ' => 'T', 'ț' => 't', 'Ț' => 'T', 'ŧ' => 't', 'Ŧ' => 'T', 'ú' => 'u', 'Ú' => 'U', 'ù' => 'u', 'Ù' => 'U', 'ŭ' => 'u', 'Ŭ' => 'U', 'û' => 'u', 'Û' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ű' => 'u', 'Ű' => 'U', 'ũ' => 'u', 'Ũ' => 'U', 'ų' => 'u', 'Ų' => 'U', 'ū' => 'u', 'Ū' => 'U', 'ư' => 'u', 'Ư' => 'U', 'ü' => 'ue', 'Ü' => 'UE', 'ẃ' => 'w', 'Ẃ' => 'W', 'ẁ' => 'w', 'Ẁ' => 'W', 'ŵ' => 'w', 'Ŵ' => 'W', 'ẅ' => 'w', 'Ẅ' => 'W', 'ý' => 'y', 'Ý' => 'Y', 'ỳ' => 'y', 'Ỳ' => 'Y', 'ŷ' => 'y', 'Ŷ' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y', 'ź' => 'z', 'Ź' => 'Z', 'ž' => 'z', 'Ž' => 'Z', 'ż' => 'z', 'Ż' => 'Z', 'þ' => 'th', 'Þ' => 'Th', 'µ' => 'u', 'а' => 'a', 'А' => 'a', 'б' => 'b', 'Б' => 'b', 'в' => 'v', 'В' => 'v', 'г' => 'g', 'Г' => 'g', 'д' => 'd', 'Д' => 'd', 'е' => 'e', 'Е' => 'E', 'ё' => 'e', 'Ё' => 'E', 'ж' => 'zh', 'Ж' => 'zh', 'з' => 'z', 'З' => 'z', 'и' => 'i', 'И' => 'i', 'й' => 'j', 'Й' => 'j', 'к' => 'k', 'К' => 'k', 'л' => 'l', 'Л' => 'l', 'м' => 'm', 'М' => 'm', 'н' => 'n', 'Н' => 'n', 'о' => 'o', 'О' => 'o', 'п' => 'p', 'П' => 'p', 'р' => 'r', 'Р' => 'r', 'с' => 's', 'С' => 's', 'т' => 't', 'Т' => 't', 'у' => 'u', 'У' => 'u', 'ф' => 'f', 'Ф' => 'f', 'х' => 'h', 'Х' => 'h', 'ц' => 'c', 'Ц' => 'c', 'ч' => 'ch', 'Ч' => 'ch', 'ш' => 'sh', 'Ш' => 'sh', 'щ' => 'sch', 'Щ' => 'sch', 'ъ' => '', 'Ъ' => '', 'ы' => 'y', 'Ы' => 'y', 'ь' => '', 'Ь' => '', 'э' => 'e', 'Э' => 'e', 'ю' => 'ju', 'Ю' => 'ju', 'я' => 'ja', 'Я' => 'ja');
        return str_replace(array_keys($transliterationTable), array_values($transliterationTable), $txt);
    }

}

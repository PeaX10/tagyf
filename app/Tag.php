<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Collection;

class Tag extends Model
{
    protected $table = 'tags';

    protected $fillable = ['id', 'slug', 'visible', 'image', 'gender', 'langs', 'created_at'];

    public function translations(){
        return $this->hasMany('App\Translation');
    }

    public function results(){
        return $this->hasMany('App\Result');
    }

    public function visits(){
        return $this->hasMany('App\Visit');
    }

    public function answers(){
        return $this->hasMany('App\Answer');
    }

    public function getTranslationAttribute(){
        if (!array_key_exists('translations', $this->relations))
            $this->load('translations');
        $related = $this->getRelation('translations')->where('lang', App::getLocale())->first();

        if(!empty($related->data)) $related = json_decode($related->data); else return false;
        $translations = new Collection();
        foreach($related as $key => $value){
            $translations->$key = $value;
        }
        return ($translations) ? $translations : 'NaN';
    }
}

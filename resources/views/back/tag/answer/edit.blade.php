@extends('back.layouts.default', ['active' => 'tag'])

@section('title')
    Modification de la réponse
@endsection

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <h4>Edition de la réponse <small><span class="label label-info">ID: {{ $answer->id }}</span></small></h4>
        <div class="card">
        <div class="content">
            {!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->action( url('tag/'.$answer->tag_id.'/answer/'.$answer->id) )->put()->enctype("multipart/form-data") !!}
            {!! BootForm::bind($answer) !!}
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! BootForm::select('Sexe', 'gender')->options(['all' => 'Homme et Femme', 'male' => 'Homme', 'female' => 'Femme']) !!}
            {!! BootForm::select('En Ligne?', 'visible')->options([false => 'Non', 1 => 'Oui']) !!}
            <div class="form-group">
                <label class="col-sm-4 col-lg-2 control-label" for="visible">Aperçu</label>
                <div class="col-sm-8 col-lg-10">
                    <img src="{{ url('uploads/answer/'.$answer->background) }}" class="img-responsive img-rounded img-raised">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 col-lg-2 control-label" for="visible">Photo</label>
                <div class="col-sm-8 col-lg-10">
                    <label><small>Taille stricte 1200x630 (8MB MAX)</small></label>
                    <input 	type='file'
                              class='input-ghost'
                              name='image'
                              style='visibility:hidden; height:0'
                              onchange="$(this).next().find('input').val(($(this).val()).split('\\').pop());">
                    <div class="input-group input-file" name="Fichier_1">
                                        <span class="input-group-btn">
                                            <button 	class="btn btn-info btn-info btn-fill btn-choose"
                                                       type="button"
                                                       onclick="$(this).parents('.input-file').prev().click();">Choisir</button>
                                        </span>
                        <input 	type="text"
                                  class="form-control"
                                  placeholder='Choisissez un fichier...'
                                  style="cursor:pointer"
                                  onclick="$(this).parents('.input-file').prev().click(); return false;"
                        />
                    </div>
                </div>
            </div>
            <hr>
            <a href="{{ url('tag/'.$answer->tag_id.'/edit') }}" class="btn btn-rounded btn-fill pull-leftt">Retour</a>
            <input type="submit" class="btn btn-rounded btn-fill btn-info pull-right" value="Modifier">
            {!! BootForm::close() !!}
        </div>
    </div>
    </div>
@endsection
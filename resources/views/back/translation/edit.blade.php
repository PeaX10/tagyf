@extends('back.layouts.default', ['active' => 'translation'])

@section('title') Traduction #{{ $trans->id }} - Lang: {{ $trans->lang }} @endsection

@section('content')
    <div class="row">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <h4>
                            Traductions <small><span class="label label-info">{{ count(4) }}</span></small>
                        </h4>
                        <hr>
                        {!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->action(url('translation/'.$trans->id))->put() !!}
                        @foreach($translations as $key => $value)
                            {!! BootForm::text($key, $key)->value($value) !!}
                        @endforeach
                        {!! BootForm::submit('Modifier')->addClass('btn-info')->addClass('btn-fill') !!}
                        {!! BootForm::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('back.layouts.default', ['active' => 'quizz'])

@section('title')
    Ajout d'un composant
@endsection

@section('css')
    <link href="{{ url('css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/admin.css') }}" rel="stylesheet">
    <style>
        .colorpicker-2x .colorpicker-saturation {
            width: 200px;
            height: 200px;
        }

        .colorpicker-2x .colorpicker-hue,
        .colorpicker-2x .colorpicker-alpha {
            width: 30px;
            height: 200px;
        }

        .colorpicker-2x .colorpicker-color,
        .colorpicker-2x .colorpicker-color div {
            height: 30px;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <h4>Modfication du composant</h4>
        <div class="card">
            <div class="content">
                {!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [3, 9]])->action( url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id.'/composant') )->enctype("multipart/form-data") !!}
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <hr>
                @endif
                <h4>Informations</h4>
                <hr>
                {!! BootForm::text('Nom', 'name') !!}
                <div class="form-group type">
                    <label class="col-sm-4 col-lg-3 control-label">Type</label>
                    <div class="col-sm-8 col-lg-9">
                        <div class="col-md-4">
                            <label class="radio">
                                <input type="radio" data-toggle="radio" name="type" value="picture" @if(old('type') == 'picture') checked @endif> <i class="fa fa-picture-o"></i>
                            </label>
                        </div>
                        <div class="col-md-4">
                            <label class="radio">
                                <input type="radio" data-toggle="radio" name="type" value="text" @if(old('type') == 'text') checked @endif> <i class="fa fa-font"></i>
                            </label>
                        </div>
                    </div>
                </div>
                <h4>Position</h4>
                <hr>
                <div class="col-sm-6">
                    {!! BootForm::text('Longueur', 'width')->type('number')->min(1)->max(1200)->placeholder('En pixel')->value(old('width') ? old('width') : 100) !!}
                </div>
                <div class="col-sm-6">
                    {!! BootForm::text('Largeur', 'height')->type('number')->min(1)->max(630)->placeholder('En pixel')->value(old('height') ? old('height') : 50) !!}
                </div>
                <div class="col-sm-6">
                    {!! BootForm::text('Gauche', 'left')->type('number')->min(0)->max(1200)->placeholder('En pixel')->value(old('left') ? old('left') : 0) !!}
                </div>
                <div class="col-sm-6">
                    {!! BootForm::text('Haut', 'top')->type('number')->min(0)->max(630)->placeholder('En pixel')->value(old('top') ? old('top') : 0) !!}
                </div>
                <div class="col-sm-6">
                    {!! BootForm::text('Angle', 'angle')->type('number')->min(-360)->max(360)->placeholder('En degre')->value(old('angle') ? old('angle') : 0) !!}
                </div>
                <div class="col-sm-6">
                    {!! BootForm::text('Index', 'index')->type('number')->min(1)->placeholder('Ordre')->value(old('index') ? old('index') : 1) !!}
                </div>
                {!! BootForm::select('Position par rapport à l\'image', 'b_o_f')->options(['front' => 'Au dessus', 'back' => 'En dessous'])->class('selectpicker') !!}
                <h4>Style</h4>
                <hr>
                <div class="content-type text hidden">
                    {!! BootForm::text('Taille de la police', 'font-size')->type('number')->min(1)->max(500)->placeholder('En pixel')->value(old('text-size') ? old('text-size') : 20) !!}
                    {!! BootForm::select('Police', 'font-family')->class("selectpicker")->options(config('settings.fonts'))->data_show_subtext(false)->data_live_search(true) !!}
                    <div class="form-group">
                        <label class="col-sm-4 col-lg-3 control-label">Couleur</label>
                        <div id="cp1" class="input-group colorpicker-component col-sm-8 col-lg-9">
                            <input type="text" value="@if(old('text-color') != ''){{ old('text-color') }}@else{{ '#FFFF' }}@endif" name="text-color" class="form-control" />
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>
                    {!! BootForm::select('Alignement vertical', 'vertical_align')->class("selectpicker")->options(['top' => 'Haut', 'middle' => 'Milieu', 'bottom' => 'Bas']) !!}
                    {!! BootForm::select('Alignement horizontal', 'horizontal_align')->class("selectpicker")->options(['left' => 'Gauche', 'center' => 'Centre', 'right' => 'Droite']) !!}
                    {!! BootForm::text('Taille contour', 'stroke-size')->type('number')->min(0)->max(360)->placeholder('En pixel')->value(old('stroke-size') ? old('stroke-size') : 0) !!}
                    <div class="form-group">
                        <label class="col-sm-4 col-lg-3 control-label">Couleur du contour</label>
                        <div id="cp2" class="input-group colorpicker-component col-sm-8 col-lg-9">
                            <input type="text" value="@if(old('stroke-color')){{ old('stroke-color') }}@else{{ '#000000' }}@endif" name="stroke-color" class="form-control" />
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>
                </div>
                <div class="content-type picture hidden">
                    <div class="form-group mask">
                        <label class="col-sm-4 col-lg-3 control-label">Masque</label>
                        <div class="col-sm-8 col-lg-9">
                            <div class="col-md-4">
                                <label class="radio">
                                    <input type="radio" data-toggle="radio" name="mask" @if(old('mask') == 'regular') checked @endif value="regular"> <img src="{{ url('img/placeholder.jpg') }}" style="max-width:50px">
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label class="radio">
                                    <input type="radio" data-toggle="radio" name="mask" @if(old('mask') == 'circle') checked @endif value="circle"> <img src="{{ url('img/placeholder.jpg') }}" class="img-circle" style="max-width:50px">
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label class="radio">
                                    <input type="radio" data-toggle="radio" name="mask" @if(old('mask') == 'custom') checked @endif value="custom"> <img src="{{ url('img/who.jpg') }}" class="" style="max-width:50px">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group custom-mask hidden">
                        <label class="col-sm-4 col-lg-3 control-label" for="visible">Masque personnalisé</label>
                        <div class="col-sm-8 col-lg-9">
                            <label><small>Même taille que l'image</small></label>
                            <input 	type='file'
                                      class='input-ghost'
                                      name='custom-mask'
                                      style='visibility:hidden; height:0'
                                      onchange="$(this).next().find('input').val(($(this).val()).split('\\').pop());">
                            <div class="input-group input-file" name="Fichier_1">
                                        <span class="input-group-btn">
                                            <button 	class="btn btn-info btn-info btn-fill btn-choose"
                                                       type="button"
                                                       onclick="$(this).parents('.input-file').prev().click();">Choisir</button>
                                        </span>
                                <input 	type="text"
                                          class="form-control"
                                          placeholder='Choisissez un masque...'
                                          style="cursor:pointer"
                                          onclick="$(this).parents('.input-file').prev().click(); return false;"
                                />
                            </div>
                        </div>
                    </div>
                    {!! BootForm::text('Opacité', 'opacity')->type('number')->min(0)->max(100)->placeholder('En %')->value(old('opacity') ? old('opacity') : 100) !!}
                    {!! BootForm::text('Flou', 'blur')->type('number')->min(0)->max(100)->placeholder('En %')->value(old('blur') ? old('blur') : 0) !!}
                    {!! BootForm::text('Luminosité', 'brightness')->type('number')->min(-100)->max(100)->placeholder('En %')->value(old('brightness') ? old('brightness') : 0) !!}
                    {!! BootForm::text('Contraste', 'contrast')->type('number')->min(-100)->max(100)->placeholder('En %')->value(old('contrast') ? old('contrast') : 0) !!}
                </div>
                <h4>Contenu</h4>
                <hr>
                <div class="content-type picture hidden">
                    <ul class="nav nav-pills nav-justified">
                        <li><a onclick="addVar('{! me.avatar !}')" href="javascript:void(0)">Mon avatar</a></li>
                        <li><a onclick="addVar('{! friend1.avatar !}')" href="javascript:void(0)">Avatar ami</a></li>
                    </ul>
                </div>
                <div class="content-type text hidden">
                    <ul class="nav nav-pills nav-justified">
                        <li><a onclick="addVar('{! me.name !}')" href="javascript:void(0)">Mon nom complet</a></li>
                        <li><a onclick="addVar('{! me.last_name !}')" href="javascript:void(0)">Mon nom</a></li>
                        <li><a onclick="addVar('{! me.first_name !}')" href="javascript:void(0)">Mon prénom</a></li>
                    </ul>
                    <ul class="nav nav-pills nav-justified">
                        <li><a onclick="addVar('{! friend1.name !}')" href="javascript:void(0)">Nom complet ami</a></li>
                        <li><a onclick="addVar('{! friend1.last_name !}')" href="javascript:void(0)">Nom ami</a></li>
                        <li><a onclick="addVar('{! friend1.first_name !}')" href="javascript:void(0)">Prénom ami</a></li>
                    </ul>
                </div>
                <br>
                {!! BootForm::text('Object', 'object')->placeholder('Enter votre texte, vos variables ou l\'url de votre image') !!}
                <div class="form-group content-type picture hidden">
                    <label class="col-sm-4 col-lg-3 control-label" for="visible">Image</label>
                    <div class="col-sm-8 col-lg-9">
                        <label><small>(8MB MAX)</small></label>
                        <input 	type='file'
                                  class='input-ghost'
                                  name='image'
                                  style='visibility:hidden; height:0'
                                  onchange="$(this).next().find('input').val(($(this).val()).split('\\').pop());">
                        <div class="input-group input-file" name="Fichier_2">
                                        <span class="input-group-btn">
                                            <button 	class="btn btn-info btn-info btn-fill btn-choose"
                                                       type="button"
                                                       onclick="$(this).parents('.input-file').prev().click();">Choisir</button>
                                        </span>
                            <input 	type="text"
                                      class="form-control"
                                      placeholder='Choisissez un fichier...'
                                      style="cursor:pointer"
                                      onclick="$(this).parents('.input-file').prev().click(); return false;"
                            />
                        </div>
                    </div>
                </div>
                <hr>
                <a href="{{ url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id) }}" class="btn btn-rounded btn-fill pull-leftt">Retour</a>
                <input type="submit" class="btn btn-rounded btn-fill btn-info pull-right" value="Créer">
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            if($('.type .radio input:checked').val() == 'picture'){
                $('.content-type.text').addClass('hidden');
                $('.content-type.picture').removeClass('hidden');
            }else if($('.type .radio input:checked').val() == 'text'){
                $('.content-type.picture').addClass('hidden');
                $('.content-type.text').removeClass('hidden');
            }
            if($('.mask .radio input:checked').val() == 'custom'){
                $('.custom-mask').removeClass('hidden');
            }else{
                $('.custom-mask').addClass('hidden');
            }
            $(".type .radio input[type=radio]" ).on( "change", function(){
                if($('.type .radio input:checked').val() == 'picture'){
                    $('.content-type.text').addClass('hidden');
                    $('.content-type.picture').removeClass('hidden');
                }else if($('.type .radio input:checked').val() == 'text'){
                    $('.content-type.picture').addClass('hidden');
                    $('.content-type.text').removeClass('hidden');
                }
            });

            $(".mask .radio input[type=radio]" ).on( "change", function(){
                if($('.mask .radio input:checked').val() == 'custom'){
                    $('.custom-mask').removeClass('hidden');
                }else{
                    $('.custom-mask').addClass('hidden');
                }
            });
            $('#cp1').colorpicker({
                customClass: 'colorpicker-2x',
                format: "hex"
            });
            $('#cp2').colorpicker({
                customClass: 'colorpicker-2x',
                format: "hex"
            });
        });

        function addVar(variable){
            input = $('input[name="object"]');
            input.val(input.val()+variable);
        }
    </script>
@endsection
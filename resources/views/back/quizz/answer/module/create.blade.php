@extends('back.layouts.default', ['active' => 'quizz'])

@section('title')
    Ajout d'un module
@endsection

@section('css')
    <link href="{{ url('css/admin.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <h4>Modfication du composant</h4>
        <div class="card">
            <div class="content">
                {!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [3, 9]])->action( url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id.'/module') )->enctype("multipart/form-data") !!}
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <hr>
                @endif
                <h4>Type</h4>
                <hr>
                {!! BootForm::select('Rôle', 'type')->options(['Mon profil Facebook' => 'Au dessus', 'back' => 'En dessous'])->class('selectpicker') !!}
                <h4>Informations</h4>
                <hr>

                </div>
                <hr>
                <a href="{{ url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id) }}" class="btn btn-rounded btn-fill pull-leftt">Retour</a>
                <input type="submit" class="btn btn-rounded btn-fill btn-info pull-right" value="Créer">
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            if($('.type .radio input:checked').val() == 'picture'){
                $('.content-type.text').addClass('hidden');
                $('.content-type.picture').removeClass('hidden');
            }else if($('.type .radio input:checked').val() == 'text'){
                $('.content-type.picture').addClass('hidden');
                $('.content-type.text').removeClass('hidden');
            }
            if($('.mask .radio input:checked').val() == 'custom'){
                $('.custom-mask').removeClass('hidden');
            }else{
                $('.custom-mask').addClass('hidden');
            }
            $(".type .radio input[type=radio]" ).on( "change", function(){
                if($('.type .radio input:checked').val() == 'picture'){
                    $('.content-type.text').addClass('hidden');
                    $('.content-type.picture').removeClass('hidden');
                }else if($('.type .radio input:checked').val() == 'text'){
                    $('.content-type.picture').addClass('hidden');
                    $('.content-type.text').removeClass('hidden');
                }
            });

            $(".mask .radio input[type=radio]" ).on( "change", function(){
                if($('.mask .radio input:checked').val() == 'custom'){
                    $('.custom-mask').removeClass('hidden');
                }else{
                    $('.custom-mask').addClass('hidden');
                }
            });
            $('#cp1').colorpicker({
                customClass: 'colorpicker-2x',
                format: "hex"
            });
            $('#cp2').colorpicker({
                customClass: 'colorpicker-2x',
                format: "hex"
            });
        });

        function addVar(variable){
            input = $('input[name="object"]');
            input.val(input.val()+' '+variable);
        }
    </script>
@endsection
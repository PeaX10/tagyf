@extends('back.layouts.default', ['active' => 'quizz'])

@section('title') Construction de la réponse @endsection

@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.ui.rotatable/1.0.1/jquery.ui.rotatable.css">
    <link rel="stylesheet" href="{{ url('css/admin.css') }}" type="text/css">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="content">
                    <h4>Aperçu de la réponse <a class="btn btn-info btn-fill pull-right" href="{{ url('quizz/'.$answer->quizz_id.'/edit') }}">Retour au quizz</a></h4>
                    <hr>
                    <figure>
                        <div class="preview img-responsive" style="background: url('{{ url('uploads/answer/preview/'.$answer->id.'.jpg') }}') no-repeat center; background-size: contain;">
                        </div>
                    </figure>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="content">
                    <h4>Modules <small><a href="{{ Request::URL() }}/module/create" class="btn btn-danger btn-round btn-fill pull-right"><i class="fa fa-plus"></i></a></small></h4>
                    <hr>
                    @if(!empty($builder->modules))

                    @else
                        <h5  align="center">Aucun module</h5>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="content">
                    <h4>Builder</h4>
                    <hr>
                    <figure>
                        <div class="preview" style="background: url('{{ url('uploads/answer/'.$answer->background) }}') no-repeat center; background-size: contain;">
                            @foreach($composants as $composant)
                                <div id="{{ $composant->id }}" class="composant {{ $composant->type }}"
                                    style="width:{{ $composant->width }}%; height: {{ $composant->height }}%; top: {{ $composant->pos_y }}%;left: {{ $composant->pos_x }}%; z-index: {{ isset($composant->index) ? $composant->index : 0 }}; transform: rotate({{ $composant->angle }}deg)">
                                    <a>{{ $composant->name }}</a>
                                </div>
                            @endforeach
                        </div>
                    </figure>
                    <hr>
                    <a class="btn btn-round btn-info pull-right btn-fill" onclick="javascript:savPos()">Sauvegarder</a>
                    <br>
                    <br>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="content">
                    <h4>Composants <small><a href="{{ url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id.'/composant/create') }}" class="btn btn-danger btn-round btn-fill pull-right"><i class="fa fa-plus"></i></a></small></h4>
                    <hr>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($composants as $composant)
                            <tr class="tr-composant">
                                <td>{{ $composant->name }}</td>
                                <td>
                                    @if($composant->type == 'text')
                                        <a rel="tooltip" data-original-title="Texte" href="javascript:void(0)"><i class="fa fa-font text-success"></i></a>
                                    @else
                                        <a rel="tooltip" data-original-title="Image" href="javascript:void(0)"><i class="fa fa-picture-o text-danger"></i></a>
                                    @endif
                                </td>
                                <td>
                                    <a rel="tooltip" title="" class="btn btn-simple btn-warning btn-icon table-action edit" href="{{ url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id.'/composant/'.$composant->id.'/edit') }}" data-original-title="Modifier"><i class="fa fa-edit"></i></a>
                                    <a rel="tooltip" title="" composant-id="{{ $composant->id }}" class="btn btn-simple btn-danger btn-icon table-action remove" href="javascript:void(0)" data-original-title="Supprimer"><i class="fa fa-remove"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if(empty($composant))
                            <h5 align="center">Aucun composant</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="{{ url('js/jquery.ui.rotatable.min.js') }}"></script>
    <script type="text/javascript">
        $('.preview').height($('.preview').width() * 0.525);

        $(window).on('resize ready', function() {
            $('.preview').height($('.preview').width() * 0.525);
        });
        $( ".composant" ).each(function( index ){
            $(this).draggable({
                containment: 'parent',
                stop:dragStop,
                stack: "div.composant"
            }).resizable({
                containment: 'parent',
                handles: "n, ne, nw, s, se, sw, e, w",
                create: setContainerResizer,
                stop:resizeStop
            }).rotatable({
                wheelRotate: false
            }).dblclick(function(){
                var id = $(this).attr('id');
                window.location.href = "{{ url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id.'/composant') }}/"+id+"/edit";
            });
        });

        function resizeStop(event, ui){
            convert_to_percenquizze($(this));
        }

        function dragStop(event, ui){
            convert_to_percenquizze($(this));
        }

        function setContainerResizer(event, ui) {
            $($(this)[0]).children('.ui-resizable-handle').mouseover(setContainerSize);
        }

        function convert_to_percenquizze(el, to_array = false){
            var parent = el.parent();
            var info = {
                left: parseInt(el.css('left'))/parent.width()*100+"%",
                top: parseInt(el.css('top'))/parent.height()*100+"%",
                width: el.width()/parent.width()*100+"%",
                height: el.height()/parent.height()*100+"%"
            };
            if(to_array){
                return info;
            }else{
                el.css(info);
            }
        }

        function setContainerSize(el) {
            var parent = $(el.target).parent().parent();
            parent.css('height', parent.height() + "px");
        }

        function getRotationDegrees(obj) {
            var matrix = obj.css("-webkit-transform") ||
                    obj.css("-moz-transform")    ||
                    obj.css("-ms-transform")     ||
                    obj.css("-o-transform")      ||
                    obj.css("transform");
            if(matrix !== 'none') {
                var values = matrix.split('(')[1].split(')')[0].split(',');
                var a = values[0];
                var b = values[1];
                var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
            } else { var angle = 0; }
            return (angle < 0) ? angle + 360 : angle;
        }

        function savPos(){
            var pos = [];
            var c_pos = [];
            $('.composant').each(function(){
                c_pos = {
                    id: $(this).attr('id'),
                    left: parseInt($(this).css('left'))/$(this).parent().width()*100,
                    top: parseInt($(this).css('top'))/$(this).parent().height()*100,
                    width: $(this).width()/$(this).parent().width()*100,
                    height: $(this).height()/$(this).parent().height()*100,
                    angle: getRotationDegrees($(this)),
                    index: $(this).css('z-index')
                };
                pos.push(c_pos);
            });
            $.ajax({
                url: '{{ Request::url() }}/pos',
                type: 'post',
                data: {_method: 'post', _token : '{{ csrf_token() }}', pos: pos },
                success: function(result) {
                    swal("Positions sauvegardées!", "Les positions des composants ont bien été sauvegardées.", "success");
                },
                error: function(error){
                    console.log(error);
                    swal("Erreur!", "Une erreur c'est produite, les positions des composants n'ont pas été sauvegardées.", "error");
                }
            });
        }
        $('.tr-composant .remove').click(function(){
            var composant = $(this).parent().parent();
            var composant_id = $(this).attr('composant-id');
            swal({  title: "Êtes-vous sûr ?",
                html: 'Vous allez supprimer ce composant<br>' +
                'En appuyant sur le bouton "Confirmer", Toutes les données issues de ce composant tel que les images utilisées ou les masques seront supprimés.',
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Confirmer",
                cancelButtonText: "Annuler",
                closeOnConfirm: false,
                closeOnCancel: false
            },function(isConfirm){
                if (isConfirm){
                    $.ajax({
                        url: '{{ url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id) }}/composant/'+ composant_id,
                        type: 'delete',
                        data: {_method: 'delete', _token : '{{ csrf_token() }}' },
                        success: function(result) {
                            swal("Supprimé!", "Le composant a bien été supprimé.", "success");
                            composant.remove();
                            $('.preview #'+composant_id).remove();
                        },
                        error: function(error){
                            console.log(error);
                            swal("Erreur!", "Une erreur c'est produite, le composant n'a pas pu être supprimé.", "error");
                        }
                    });
                }else{
                    swal("Annulé", "Ouff... Le composant est encore là :)", "error");
                }
            });
        });
        @if(Session::has('added_composant'))
            $.notify({
            icon: 'pe-7s-bell',
            message: "<b>Félicitation</b> - votre composant a bien été créer."

        },{
            type: 'success',
            timer: 4000
        });
        @endif
        @if(Session::has('updated_composant'))
            $.notify({
            icon: 'pe-7s-bell',
            message: "<b>Félicitation</b> - votre composant a bien été mis à jour."

        },{
            type: 'success',
            timer: 4000
        });
        @endif
        /*
        var myVar=setInterval(function(){
            $('#info').html('LEFT : '+$('.composant')[0].style.left + '<br />TOP:  '+$('.composant')[0].style.top + '<br />WIDTH: '+$('.composant')[0].style.width + '<br />HEIGHT: '+$('.composant')[0].style.height);
        },10);
        */
    </script>
@endsection
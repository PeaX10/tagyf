@extends('back.layouts.default', ['active' => 'quizz'])

@section('title') Modification quizz #{{ $quizz->slug }} @endsection

@section('content')
    <div class="row">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content answer">
                        <h4>
                            Réponses possibles <small><span class="label label-info">{{ count($answers) }}</span></small>
                            <a class="pull-right btn btn-round btn-danger btn-fill" href="{{ url('quizz/'.$quizz->id.'/answer/create') }}"><i class="fa fa-plus"></i></a>
                        </h4>
                        <hr>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Fond</th>
                                    <th>En Ligne?</th>
                                    <th>Sexe</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($answers as $answer)
                                    <tr>
                                        <td>{{ $answer->id }}</td>
                                        <td><img src="@if(file_exists(public_path('uploads/answer/preview/'.$answer->id.'.jpg'))){{ url('uploads/answer/preview/'.$answer->id.'.jpg') }}@else{{ url('uploads/answer/'.$answer->background) }}@endif" class="img-responsive img-raised img-rounded" style="max-width: 400px"></td>
                                        <td>@if($answer->visible) <span class="label label-success">Oui</span> @else <span class="label label-danger">Non</span> @endif</td>
                                        <td>
                                            @if($answer->gender == 'male')
                                                <i class="fa fa-mars"></i>
                                            @elseif($answer->gender == 'female')
                                                <i class="fa fa-venus"></i>
                                            @else
                                                <i class="fa fa-venus-mars"></i>
                                            @endif
                                        </td>
                                        <td>
                                            <a rel="tooltip" title="" class="btn btn-simple btn-success btn-icon table-action builder" href="{{ url('quizz/'.$quizz->id.'/answer/'.$answer->id.'/clone') }}" data-original-title="Dupliquer"><i class="fa fa-clone"></i></a>
                                            <a rel="tooltip" title="" class="btn btn-simple btn-info btn-icon table-action builder" href="{{ url('quizz/'.$quizz->id.'/answer/'.$answer->id) }}" data-original-title="Construire"><i class="fa fa-puzzle-piece"></i></a>
                                            <a rel="tooltip" title="" class="btn btn-simple btn-warning btn-icon table-action edit" href="{{ url('quizz/'.$quizz->id.'/answer/'.$answer->id.'/edit') }}" data-original-title="Modifier"><i class="fa fa-edit"></i></a>
                                            <a rel="tooltip" title="" answer-id="{{ $answer->id }}" class="btn btn-simple btn-danger btn-icon table-action remove" href="javascript:void(0)" data-original-title="Supprimer"><i class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="content lang">
                        <h4>
                            Langues du quizz @if($langs[0] != '')<small><span class="label label-info">{{ count($langs) }}</span></small>@endif
                            <a quizz-id="{{ $quizz->id }}" class="pull-right btn btn-round btn-danger btn-fill add" href="javascript:void(0)"><i class="fa fa-plus"></i></a>
                        </h4>
                        <hr>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Langue</th>
                                    <th>En Ligne?</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($langs as $lang)
                                    @if($lang != '')
                                    <tr>
                                        <td><img src="{{ url('img/flags/'.$lang.'.png') }}"></td>
                                        <td>
                                            <?php
                                            $t_lang = $quizz->translations()->where('lang', $lang)->first();
                                            ?>
                                            @if($t_lang != null && $t_lang->visible)
                                                <span class="label label-success">Oui</span>
                                            @else
                                                <span class="label label-danger">Non</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a rel="tooltip" title="" class="btn btn-simple btn-info btn-icon table-action trans" href="{{ url('quizz/'.$quizz->id.'/lang/'.$lang) }}" data-original-title="Traduire"><i class="fa fa-language"></i></a>
                                            @if($t_lang != null && $t_lang->visible)
                                                <a rel="tooltip" title="" class="btn btn-simple btn-success btn-icon table-action offline" href="{{ url('quizz/'.$quizz->id.'/lang/'.$lang.'/edit') }}" data-original-title="Mettre hors-ligne"><i class="fa fa-toggle-on"></i></a>
                                            @else
                                                <a rel="tooltip" title="" class="btn btn-simple btn-warning btn-icon table-action online" href="{{ url('quizz/'.$quizz->id.'/lang/'.$lang.'/edit') }}" data-original-title="Mettre en ligne"><i class="fa fa-toggle-off"></i></a>
                                            @endif
                                            <a rel="tooltip" lang-quizz="{{ $lang }}" title="" class="btn btn-simple btn-danger btn-icon table-action remove" href="javascript:void(0)" data-original-title="Supprimer"><i class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="content">
                        <h3>
                            @if(!empty($quizz->translation->title))
                                {{ $quizz->translation->title }}
                            @else
                                {{ $quizz->slug }}
                            @endif
                            <small><span class="label label-info">ID: {{ $quizz->id }}</span></small>
                        </h3>
                        <hr>
                        <div clas="col-md-10 col-md-offset-1">
                            {!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->action( route('quizz.update', $quizz) )->put()->enctype("multipart/form-data") !!}
                            {!! BootForm::bind($quizz) !!}
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! BootForm::text('Slug', 'slug') !!}
                            {!! BootForm::select('Type', 'type')->options(['friend' => 'Ami', 'random' => 'Aléatoire']) !!}
                            {!! BootForm::select('Sexe', 'gender')->options(['all' => 'Homme et Femme', 'male' => 'Homme', 'female' => 'Femme']) !!}
                            {!! BootForm::select('En Ligne?', 'visible')->options([false => 'Non', 1 => 'Oui']) !!}
                            <div class="form-group">
                                <label class="col-sm-4 col-lg-2 control-label" for="visible">Aperçu</label>
                                <div class="col-sm-8 col-lg-10">
                                    <img src="{{ url('uploads/quizz/'.$quizz->image) }}" class="img-responsive img-rounded img-raised">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 col-lg-2 control-label" for="visible">Photo</label>
                                <div class="col-sm-8 col-lg-10">
                                    <label><small>Taille stricte 1200x630 (8MB MAX)</small></label>
                                    <input 	type='file'
                                              class='input-ghost'
                                              name='image'
                                              style='visibility:hidden; height:0'
                                              onchange="$(this).next().find('input').val(($(this).val()).split('\\').pop());">
                                    <div class="input-group input-file" name="Fichier_1">
                                        <span class="input-group-btn">
                                            <button 	class="btn btn-info btn-info btn-fill btn-choose"
                                                       type="button"
                                                       onclick="$(this).parents('.input-file').prev().click();">Choisir</button>
                                        </span>
                                        <input 	type="text"
                                                  class="form-control"
                                                  placeholder='Image présentation du quizz'
                                                  style="cursor:pointer"
                                                  onclick="$(this).parents('.input-file').prev().click(); return false;"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 col-lg-2 control-label" for="visible">Aperçu</label>
                                <div class="col-sm-8 col-lg-10">
                                    <img src="{{ url('uploads/quizz/'.$quizz->playImage) }}" class="img-responsive img-rounded img-raised">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 col-lg-2 control-label" for="visible">Photo</label>
                                <div class="col-sm-8 col-lg-10">
                                    <label><small>Taille stricte 1200x630 (8MB MAX)</small></label>
                                    <input 	type='file'
                                              class='input-ghost'
                                              name='playImage'
                                              style='visibility:hidden; height:0'
                                              onchange="$(this).next().find('input').val(($(this).val()).split('\\').pop());">
                                    <div class="input-group input-file" name="Fichier_1">
                                        <span class="input-group-btn">
                                            <button 	class="btn btn-info btn-info btn-fill btn-choose"
                                                       type="button"
                                                       onclick="$(this).parents('.input-file').prev().click();">Choisir</button>
                                        </span>
                                        <input 	type="text"
                                                  class="form-control"
                                                  placeholder='Image page du quizz'
                                                  style="cursor:pointer"
                                                  onclick="$(this).parents('.input-file').prev().click(); return false;"
                                        />
                                    </div>
                                </div>
                            </div>
                            {!! BootForm::submit('Modifier')->class('btn btn-rounded btn-fill pull-right') !!}
                            {!! BootForm::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="content">
                        <h4>Les 30 dernières visites <small><span class="label label-info">total: {{ count($visits) }}</span></small></h4>
                        <hr>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Utilisateur</th>
                                    <th>Langue</th>
                                    <th>IP</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($visits->take(30) as $visit)
                                <tr>
                                    <td>{{ $visit->id }}</td>
                                    @if($visit->user()->exists())
                                        <td><a href="{{ url('user/'.$visit->user()->first()->id.'/edit') }}">{{ $visit->user()->first()->name }}</a></td>
                                    @else
                                        <td><a href="#">Inconnu</a></td>
                                    @endif
                                    <td><img src="{{ url('img/flags/'.$visit->lang.'.png') }}"></td>
                                    <td>{{ $visit->ip }}</td>
                                    <td>{{ \Carbon\Carbon::parse($visit->created_at)->diffForHumans() }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="content">
                        <h4>Les 30 derniers résultats générés <small><span class="label label-info">{{ count($results) }}</span></small></h4>
                        <hr>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Utilisateur</th>
                                    <th>Image</th>
                                    <th>Langue</th>
                                    <th>IP</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($results->take(30) as $result)
                                    <tr>
                                        <td>{{ $result->id }}</td>
                                        @if($result->user()->exists())
                                            <td><a href="{{ url('user/'.$result->user()->first()->id.'/edit') }}">{{ $result->user()->first()->name }}</a></td>
                                        @else
                                            <td><a>Compte supprimé</a></td>
                                        @endif
                                        <td><img style="max-width: 150px" src="{{ url('uploads/result/'.$result->image) }}" class="img-responsive img-raised img-rounded"></td>
                                        <td><img src="{{ url('img/flags/'.$result->lang.'.png') }}"></td>
                                        <td>{{ $result->ip }}</td>
                                        <td>{{ \Carbon\Carbon::parse($result->created_at)->diffForHumans() }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        @if(Session::has('edited_quizz'))
            $.notify({
            icon: 'pe-7s-bell',
            message: "<b>Félicitation</b> - votre quizz a bien été modifié."

        },{
            type: 'success',
            timer: 4000
        });
        @endif
        $(document).ready(function(){
            $('.answer .remove').click(function(){
                var answer = $(this).parent().parent();
                var answer_id = $(this).attr('answer-id');
                swal({  title: "Êtes-vous sûr ?",
                    html: 'Vous allez supprimer la réponse <span class="label label-info">ID: '+ answer_id +'</span></b><br>' +
                    'En appuyant sur le bouton "Confirmer", Toutes les données issues de cette réponse seront supprimées ainsi que des ses composants.',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Confirmer",
                    cancelButtonText: "Annuler",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: '{{ url('quizz/'.$quizz->id) }}/answer/'+ answer_id,
                            type: 'delete',
                            data: {_method: 'delete', _token : '{{ csrf_token() }}' },
                            success: function(result) {
                                swal("Supprimé!", "La réponse a bien été supprimé.", "success");
                                answer.remove();
                                var nb = parseInt($('.answer h4 small .label').text());
                                $('.answer h4 small .label').text(nb - 1);
                            },
                            error: function(error){
                                console.log(error);
                                swal("Erreur!", "Une erreur c'est produite, la réponse n'a pas pu être supprimé.", "error");
                            }
                        });
                    }else{
                        swal("Annulé", "Ouff... La réponse est encore là :)", "error");
                    }
                });
            });
            $('.lang .remove').click(function(){
                var lang = $(this).parent().parent();
                var lang_quizz = $(this).attr('lang-quizz');
                swal({  title: "Êtes-vous sûr ?",
                    html: 'Vous allez supprimer la langue <span class="label label-info">'+ lang_quizz +'</span></b><br>' +
                    'En appuyant sur le bouton "Confirmer", Toutes les données issues de cette langues seront supprimées ainsi que des ses traductions.',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Confirmer",
                    cancelButtonText: "Annuler",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: '{{ url('quizz/'.$quizz->id) }}/lang/'+ lang_quizz,
                            type: 'delete',
                            data: {_method: 'delete', _token : '{{ csrf_token() }}' },
                            success: function(result) {
                                swal("Supprimé!", "La langue a bien été supprimé.", "success");
                                lang.remove();
                                var nb = parseInt($('.lang h4 small .label').text());
                                $('.lang h4 small .label').text(nb - 1);
                            },
                            error: function(error){
                                console.log(error);
                                swal("Erreur!", "Une erreur c'est produite, la langue n'a pas pu être supprimé.", "error");
                            }
                        });
                    }else{
                        swal("Annulé", "Ouff... La langue est encore là :)", "error");
                    }
                });
            });

            $('.lang .add').click(function(){
                var quizz_id = $(this).attr('quizz-id');
                swal({
                    title: 'Ajouter une langue',
                    html: '<div class="form-group" id="form-add-lang">'+
                                '<label class="col-sm-4 col-lg-2 control-label">Langue</label>'+
                                '<div class="col-sm-8 col-lg-10">'+
                                    @foreach(config('settings.langs') as $key => $lang)
                                    '<label class="radio-inline">'+
                                        '<input type="radio" name="lang" value="{{ $key }}"><img src="{{ url('img/flags') }}/{{ $key }}.png" />'+
                                    '</label>'+
                                    @endforeach
                                '</div>'+
                            '</div>',
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Confirmer",
                    allowOutsideClick: false
                },
                function() {
                    $.ajax({
                        url: '{{ url('quizz/'.$quizz->id) }}/lang',
                        type: 'post',
                        data: {_method: 'post', _token : '{{ csrf_token() }}', quizz_id: '{{ $quizz->id }}', lang: $('input[name=lang]:checked', '#form-add-lang').val() },
                        success: function(result) {
                            result = $.parseJSON(result);
                            swal("Ajouté!", "La langue a bien été ajouté.", "success");
                            $('.lang tbody').append('<tr>' +
                                    '<td><img src="{{ url('img/flags') }}/'+ result.lang +'.png" ></td>' +
                                    '<td><span class="label label-danger">Non</span></td>' +
                                    '<td>' +
                                    '<a rel="tooltip" title="" class="btn btn-simple btn-info btn-icon table-action trans" href="{{ url('quizz/'.$quizz->id.'/lang') }}/'+ result.id +'" data-original-title="Traduire"><i class="fa fa-language"></i></a>'+
                                    '<a rel="tooltip" title="" class="btn btn-simple btn-warning btn-icon table-action online" href="{{ url('quizz/'.$quizz->id.'/lang') }}/'+ result.lang +'/edit" data-original-title="Mettre en ligne"><i class="fa fa-toggle-off"></i></a>'+
                                    '<a rel="tooltip" lang-id="'+ result.id +'" lang-quizz="'+ result.lang +'" title="" class="btn btn-simple btn-danger btn-icon table-action remove" href="javascript:void(0)" data-original-title="Supprimer"><i class="fa fa-remove"></i></a>'+
                                    '</td>' +
                                    '</tr>');
                            $('.lang h4 small .label').text(parseInt($('.lang h4 small .label').text()) + 1);
                        },
                        error: function(error){
                            console.log(error);
                            swal("Erreur!", "Une erreur c'est produite, la langue n'a pas pu être ajouté.", "error");
                        }
                    });
                });
            });

            @if(Session::has('answer_added'))
            $.notify({
                icon: 'pe-7s-bell',
                message: "<b>Félicitation</b> - Le status de la langue <span class='label label-success'>{{ Session::get('lang_added') }}</span> a bien été modifié."

            },{
                type: 'success',
                timer: 4000
            });
            @endif
        });
    </script>
@endsection
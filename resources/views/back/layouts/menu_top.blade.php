<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">@yield('title')</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{ strtolower('http://'.App::getLocale().'.'.env('APP_DOMAIN')) }}" class="text-danger">
                        <i class="fa fa-home"></i>
                        <p>Retour au site</p>
                    </a>
                </li>

                <li>
                    <a href="{{ url('stats') }}">
                        <i class="fa fa-line-chart"></i>
                        <p>Stats</p>
                    </a>
                </li>

                <li class="dropdown dropdown-with-icons">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-list"></i>
                        <p class="hidden-md hidden-lg">
                            More
                            <b class="caret"></b>
                        </p>
                    </a>
                    <ul class="dropdown-menu dropdown-with-icons">
                        <li>
                            <a href="{{ url('quizz/create') }}" class="text-info">
                                <i class="fa fa-plus-circle"></i> Créer un quizz
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('tag/create') }}" class="text-info">
                                <i class="fa fa-plus-circle"></i> Créer un tag
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ url('settings') }}">
                                <i class="pe-7s-tools"></i> Paramètres
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ url('lock') }}">
                                <i class="pe-7s-lock"></i> Vérouiller
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('logout') }}">
                                <i class="pe-7s-close-circle"></i>
                                Déconnexion
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</nav>
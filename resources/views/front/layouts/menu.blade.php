<nav class="navbar navbar-white" role="navigation-demo" id="navbar">
    <div class="container">

        <div class="navbar-header">
            <a href="{{ url('/') }}">
                <div class="logo-container">
                    <div class="logo">
                        <img src="{{ url('img/logo-navbar.png') }}" alt="Logo MEGATAG">
                    </div>

                </div>
            </a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li style="padding-right:15px">
                <div class="fb-like" style="line-height:30px" data-href="https://www.facebook.com/MegaTag-En-1865021723751118/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                    <img src="{{ url('img/flags/'.App::getLocale().'.png') }}">
                    <span class="hidden-sm hidden-xs">{{ config('settings.langs')[App::getLocale()] }}</span>
                    <span class="hidden-lg hidden-md">{{ strtoupper(@App::getLocale()) }}</span>
                    <b class="caret"></b>
                    <div class="ripple-container"></div></a>
                <ul class="dropdown-menu">
                    @foreach(config('settings.langs') as $key => $lang)
                        <li><a href="http://{{ $key }}.tagyf.com"><img src="{{ url('img/flags/'.$key.'.png') }}"> {{ $lang }}</a></li>
                    @endforeach
                    @if(count(config('settings.soon_langs')) > 0)
                        <li class="divider"></li>
                    @endif
                    @foreach(config('settings.soon_langs') as $key => $lang)
                        <li><a href="#"><img src="{{ url('img/flags/'.$key.'.png') }}"> {{ $lang }} <span class="label label-default">@lang('app.menu_soon')</span></a></li>
                    @endforeach
                </ul>
            </li>
        </ul>
    </div>
</nav>
<footer class="footer footer-black">

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-6 col-sm-12">
                <div class="links">
                    <ul>
                        <li>
                            <a href="{{ url('terms') }}">
                                @lang('app.footer_terms')
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('privacy') }}">
                                @lang('app.footer_privacy')
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('legal') }}">
                                @lang('app.footer_legal')
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('delete') }}">
                                @lang('app.footer_delete')
                            </a>
                        </li>
                    </ul>
                    <hr>
                    <div class="copyright">
                        <div class="pull-left">
                            © 2017 MEGATAG
                        </div>
                        <div class="pull-right">
                            <ul>
                                <li>
                                    <a href="@lang('app.twitter_link')" target="_blank" class="btn btn-just-icon btn-simple">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="@lang('app.facebook_link')" target="_blank" class="btn btn-just-icon btn-simple">
                                        <i class="fa fa-facebook-square"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="@lang('app.instagram_link')" target="_blank" class="btn btn-just-icon btn-simple">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
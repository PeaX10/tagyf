<!doctype html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ url('/img/icons/favicon-96x96.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('/img/icons/apple-icon-180x180.png') }}">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="description" content="@lang('app.meta_description')">
    <meta name="keywords" content="@lang('app.meta_keywords')">
    <meta name="author" content="PeaX">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    @yield('meta')

    <title>@yield('title')</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="{{ url('bootstrap3/css/bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ url('/css/ui.css') }}" rel="stylesheet"/>
    <link href="{{ url('/css/app.css') }}" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="{{ url('/css/themify-icons.css') }}" rel="stylesheet" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <script type="text/javascript">
        window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='//rec.getsmartlook.com/recorder.js';h.appendChild(c);
        })(document);
        smartlook('init', 'a424aa7067b4ccdd735cbd851b7c3747d8b9241b');
    </script>
</head>
<body>
@include('front.layouts.menu')
@include('front.layouts.alerts')
<div class="wrapper">
    @yield('content')
</div>
<div id="fb-root"></div>
<div class="load-result fb" style="display: none; background: rgba(255, 255, 255, 0.92); height: 100%; width: 100%; position: fixed; z-index: 1100; top:0; left: 0">
    <div class="vertical-center">
        <div class="text-center col-md-4 col-md-offset-4">
            <h2 class="title">@lang('app.loading_fb')<span id="loading">.</span></h2>
            <br>
            <img src="{{ url('img/loader.gif') }}" alt="Loader TagYF" style="max-width: 128px"/>
        </div>
    </div>
</div>
<div class="load-result tag" style="display: none; background: rgba(255, 255, 255, 0.92); height: 100%; width: 100%; position: fixed; z-index: 1100; top:0; left: 0">
    <div class="vertical-center">
        <div class="text-center col-md-4 col-md-offset-4">
            <h2 class="title">@lang('app.loading_tag')<span id="loading">.</span></h2>
            <br>
            <img src="{{ url('img/loader.gif') }}" alt="Loader TagYF" style="max-width: 128px"/>
        </div>
    </div>
</div>
@include('front.layouts.footer')
</body>
<script src="{{ url('/js/jquery-1.10.2.js') }}" type="text/javascript"></script>
<script src="{{ url('/js/jquery-ui-1.10.4.custom.min.js') }}" type="text/javascript"></script>

<script src="{{ url('bootstrap3/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ url('/js/bootstrap-select.js') }}"></script>
<script src="{{ url('/js/ui.js') }}"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-81009592-1', 'auto');
    ga('send', 'pageview');
</script>
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.7&appId=466257920209082";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
@yield('js')
<script>
    $('.load-result').hide();
    var nb = 1;
    $('#fbLogin, #syncFriend').on('click', function(){
        $('.load-result.fb').show();
        setInterval(function(){
            nb += 1;
            if(nb <= 3){
                $("span#loading").append(".");
            }else{
                nb = 1;
                $("span#loading").html(".");
            }
        }, 250);
    });
    $('input[type="submit"], .card-footer .btn-warning, #doQuizz').on('click', function(){
        $('.load-result.tag').show();
        setInterval(function(){
            nb += 1;
            if(nb <= 3){
                $("span#loading").append(".");
            }else{
                nb = 1;
                $("span#loading").html(".");
            }
        }, 250);
    });
</script>
<script type="text/javascript">
    window.cookieconsent_options = {"message":"@lang('app.cookie_message')","dismiss":"@lang('app.cookie_dismiss')","learnMore":"@lang('app.cookie_learMore')","link":"https://fr.tagyf.com/privacy","theme":"light-bottom"};
</script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.10/cookieconsent.min.js"></script>
</html>
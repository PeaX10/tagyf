@extends('front.layouts.default')

@section('title') {{ $pagetag->translation->title }} - TagYF @endsection

@section('content')
    <div class="section section-white-gray">
        <div class="container">
            <div class="col-md-8">
                <div class="card tag" data-background="color" data-color="white">
                    <div class="content text-center">
                        <h4 class="title text-primary">{{ $pagetag->translation->title }}</h4>
                        <img src="{{ url('uploads/tag/'.$pagetag->image) }}" alt="Tag : {{ $pagetag->translation->title }} - TagYF">
                        <hr>
                    </div>
                    <div class="card-footer" align="center">
                        @if(Auth::check())
                            <a class="btn btn-warning btn-fill btn-block" href="{{ url('d/'.$pagetag->slug.'/luck/'.$friend->name) }}"><img src="{{ $friend->picture }}" class="img-circle" style="margin-bottom:0px"> <span class="name"> @lang('app.to_tag') {{ $friend->name }}</span></a>
                            <hr>
                            <a href="{{ url('t/'.$pagetag->slug) }}/luck" class="btn btn-primary btn-fill btn-rotate"><i class="fa fa-repeat"></i><span class="hidden-xs"> @lang('app.do_lucky_retry')</span></a>
                            <a href="{{ url('t/'.$pagetag->slug) }}" class="btn btn-info btn-fill btn-magnify"><i class="fa fa-search"></i><span class="hidden-xs"> @lang('app.do_search_friend')</span></a>
                        @else
                            <a class="btn btn-facebook btn-fill btn-magnify btn-block" href="{{ url('auth/facebook') }}"><i class="fa fa-facebook"></i><span class="hidden-xs"> @lang('app.must_be_logged')</span></a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4 items-row hidden-xs hidden-sm">
                @foreach($tags->random(2) as $tag)
                    <div class="col-md-12">
                        <div class="card card-hover-effect" data-background="color" data-color="nude-gray">
                            <a href="{{ url('t/'.$tag->slug) }}">
                                <div class="header header-with-icon" style="background: url('{{ url('uploads/tag/'.$tag->image) }}') no-repeat center / cover">
                                </div>
                                <div class="content text-center">
                                    <h6 class="title"><a href="{{ url('t/'.$tag->slug) }}">{{ $tag->translation->title }}</a></h6>
                                </div>
                                <div class="card-footer" align="center">
                                    <a href="{{ url('t/'.$tag->slug) }}" class="btn btn-icon btn-fill btn-primary btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_tag')"><i class="fa fa-tags"></i></a>
                                    <a href="{{ url('t/'.$tag->slug) }}/luck" class="btn btn-icon btn-fill btn-info btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_lucky_tag')"><i class="fa fa-star"></i></a>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="section section-gray">
        <div class="container">
            <div class="row items-row col-md-12">
                @foreach($tags as $tag)
                    <div class="col-md-4 col-sm-6 item">
                        <div class="card card-hover-effect" data-background="color" data-color="nude-gray">
                            <a href="{{ url('t/'.$tag->slug) }}">
                                <div class="header header-with-icon" style="background: url('{{ url('uploads/tag/'.$tag->image) }}') no-repeat center / cover">
                                </div>
                                <div class="content text-center">
                                    <h6 class="title"><a href="{{ url('t/'.$tag->slug) }}">{{ $tag->translation->title }}</a></h6>
                                </div>
                                <div class="card-footer" align="center">
                                    <a href="{{ url('t/'.$tag->slug) }}" class="btn btn-icon btn-fill btn-primary btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_tag')"><i class="fa fa-tags"></i></a>
                                    <a href="{{ url('t/'.$tag->slug) }}/luck" class="btn btn-icon btn-fill btn-info btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_lucky_tag')"><i class="fa fa-star"></i></a>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        {{ $tags->render() }}
    </div>
@endsection

@section('js')
    <script src="{{ url('js/bootstrap3-typeahead.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('js/jquery.infinitescroll.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).ready(function() {
                $('.items-row').infinitescroll({
                    loading: {
                        finishedMsg: "<br><div class='end-msg'>@lang('app.loading_no_tag')</div>",
                        msgText: '<div class="col-md-4 col-md-offset-4 text-center"><div class="preloader"><div class="uil-reload-css" style=""><div></div></div><h5>@lang('app.loading')</h5></div></div></div>',
                        img: "{{ url('img/blank.png') }}"
                    },
                    navSelector: "ul.pager",
                    nextSelector: "ul.pager a[rel='next']",
                    itemSelector: ".item",
                    extraScrollPx: 50,
                    bufferPx     : 40,
                    errorCallback: function(){},
                });
            });
        });
    </script>
@endsection
@extends('front.layouts.default')

@section('title')
    @lang('app.privacy') - MEGATAG
@endsection

@section('content')
    <div class="section section-nude-gray">
        <div class="container">
            @lang('app.html_privacy')
        </div>
    </div>
@endsection
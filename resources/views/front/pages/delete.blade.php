@extends('front.layouts.default')

@section('title')
    @lang('app.delete_account') - MEGATAG
@endsection

@section('content')
    <div class="section section-nude-gray">
        <div class="container" align="center">
            <h2>@lang('app.delete_account')</h2>
            <hr>
            @if(Auth::check())
                <p>@lang('app.description_delete')</p>
                <a href="{{ url('delete/confirm') }}" class="btn btn-danger btn-lg btn-fill"><i class="fa fa-trash"></i> @lang('app.confirm')</a>
            @else
                @lang('app.must_be_logged')
            @endif
        </div>
    </div>
@endsection
@extends('front.layouts.default')

@section('title') {{ $user->name }} - MEGATAG @endsection

@section('content')
    <div class="section section-nude-gray" align="center">
        <div class="container">
            <h3>{{ $user->name }}</h3>
            <hr>
            <img src="{{ url('uploads/avatar/'.$user->avatar) }}" alt="{{ $user->name }} - TagYF" class="img-responsive img-circle" style="max-width: 250px">
            <hr>
            <a href="{{ url('logout') }}" class="btn btn-danger btn-fill btn-round">@lang('app.logout')</a>
        </div>
    </div>
@endsection
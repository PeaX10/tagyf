@extends('front.layouts.default')

@section('title')
    MEGATAG - @lang('app.home_title')
@endsection

@section('content')
    @if(count($tags))
    <div class="main">
        <div class="section text-center landing-section">
            <div class="container">
                Derniers Tags
                <hr>
                <div class="row items-row">
                    @foreach($tags as $tag)
                    <div class="col-md-4 col-sm-6 item">
                        <div class="card card-hover-effect" data-background="color" data-color="nude-gray">
                            <a href="{{ url('q/'.$tag->slug) }}">
                                <div class="header header-with-icon" style="background: url('{{ url('uploads/quizz/'.$tag->image) }}') no-repeat center / cover">
                                </div>
                                <div class="content text-center">
                                    <h6 class="title"><a href="{{ url('q/'.$tag->slug) }}">{{ $tag->translation->title }}</a></h6>
                                </div>
                                <div class="card-footer">
                                    @if($tag->type == 'friend')
                                        <a href="{{ url('q/'.$tag->slug) }}" class="btn btn-icon btn-fill btn-primary btn-block btn-magnify"><i class="fa fa-users"></i> @lang('app.choose_friend')</a>
                                    @else
                                        <a href="{{ url('q/'.$tag->slug) }}" class="btn btn-icon btn-fill btn-primary btn-block btn-move-right">@lang('app.do_quizz') <i class="ti-angle-right"></i></a>
                                    @endif
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
                {{ $tags->render() }}
            </div>
        </div>
    </div>
    @else
          <div class="section section-white-gray text-center">
              <h2>@lang('app.sorry')</h2>
              <h5><p>
                      @if(Request::isMethod('post'))
                          @lang('app.sorry_no_quizz_search')
                  </p>
              </h5>
              <hr>
              <h5>  <p>
                          <form role="search" class="form-inline" method="POST" action="{{ url('/') }}">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              <div class="form-group">
                                  <input type="text" name="tag" class="form-control border-input" placeholder="ou rechercher un quizz" @if(isset($search_tag))value="{{ $search_tag }}@endif">
                              </div>
                              <button type="submit" class="btn btn-icon btn-fill"><i class="fa fa-search"></i></button>
                          </form>
                      @else
                        @lang('app.sorry_no_quizz')
                      @endif
                  </p></h5>
          </div>
    @endif
@endsection

@section('js')
    <script src="{{ url('js/jquery.infinitescroll.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).ready(function() {
                $('.items-row').infinitescroll({
                    loading: {
                        finishedMsg: "<br><div class='end-msg'>@lang('app.loading_no_quizz')</div>",
                        msgText: '<div class="col-md-4 col-md-offset-4 text-center"><div class="preloader"><div class="uil-reload-css" style=""><div></div></div><h5>@lang('app.loading')</h5></div></div></div>',
                        img: "{{ url('img/blank.png') }}"
                    },
                    navSelector: "ul.pager",
                    nextSelector: "ul.pager a[rel='next']",
                    itemSelector: ".item",
                    extraScrollPx: 50,
                    bufferPx     : 40,
                    errorCallback: function(){},
                });
            });
        });
    </script>
@endsection
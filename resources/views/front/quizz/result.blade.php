@extends('front.layouts.default')

@section('meta')
    <meta property="fb:app_id" content="1251404328212801">
    <meta property="og:site_name" content="TagYF.com">
    <meta property="og:type" content="website">
    <meta property="og:title"         content="{{ $result->quizz->translation->title }}" />
    <meta property="og:description"   content="@if($result->quizz->type == 'friend') @lang('app.share_description_friend') @else @lang('app.share_description_random')" @endif />
    <meta property="og:url"           content="{{ Request::url() }}" />
    <meta property="og:image"         content="{{ url('uploads/result/'.$result->image) }}" />
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <meta property="og:locale" content="{{ App::getLocale() }}">
    <meta name="twitter:card" content="photo">
    <meta name="twitter:title" content="{{ $result->quizz->translation->title }}">
    <meta name="twitter:image" content="{{ url('uploads/result/'.$result->image) }}">
    <meta name="author" content="{{ $result->user->name }}">
@endsection
@section('title') {{ $result->quizz->translation->title }} - TagYF @endsection

@section('content')
    <div class="section section-white-gray">
        <div class="container">
            <div class="col-md-8">
                <div class="card quizz" data-background="color" data-color="white">
                    <div class="content text-center">
                        <div class="hidden-xs">
                            <div class="row share">
                                <div class="col-md-10" align="center">
                                    <a class="btn btn-facebook btn-fill btn-lg btn-block btn-magnify" href="#" onclick="Share('https://www.facebook.com/sharer/sharer.php?u={{ url('qr/'.$result->slug) }}', '{{ $result->slug }}')"><i class="fa fa-facebook-square"></i> @lang('app.quizz_share_fb')</a>
                                </div>
                                <div class="col-md-2" align="center">
                                    <a class="btn btn-twitter btn-fill btn-lg btn-block btn-magnify" href="#" onclick="Share('https://twitter.com/intenq/tweet?original_referer={{ Request::url() }}&text={{ $result->quizz->translation->title }} {{ Request::url() }}', '{{ $result->slug }}')"><i class="fa fa-twitter-square"></i><span class="hidden-md hidden-lg"> @lang('app.quizz_share_twitter')</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="hidden-lg hidden-md hidden-sm">
                            <div class="row share">
                                <div class="col-xs-6" align="center">
                                    <a class="btn btn-facebook btn-fill btn-lg btn-icon btn-magnify" href="#" onclick="Share('https://www.facebook.com/sharer/sharer.php?u={{ url('qr/'.$result->slug) }}', '{{ $result->slug }}')"><i class="fa fa-facebook-square"></i> </a>
                                </div>
                                <div class="col-xs-6" align="center">
                                    <a class="btn btn-twitter btn-fill btn-lg btn-icon btn-magnify" href="#" onclick="Share('https://twitter.com/intenq/tweet?original_referer={{ Request::url() }}&text={{ $result->quizz->translation->title }} {{ Request::url() }}', '{{ $result->slug }}')"><i class="fa fa-twitter-square"></i><span class="hidden-md hidden-lg"> </span></a>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h4 class="title text-primary">{{ $result->quizz->translation->title }}</h4>
                        <img src="{{ url('uploads/result/'.$result->image) }}" alt="Quizz : {{ $result->quizz->translation->title }} - TagYF">
                        <h5>{{ $result->quizz->translation->resultDescription }}</h5>
                        <hr>
                    </div>
                    <div class="card-footer" align="center">
                        <div class="row share">
                            <div class="col-md-10" align="center">
                                <a class="btn btn-facebook btn-fill btn-lg btn-block btn-magnify" href="#" onclick="Share('https://www.facebook.com/sharer/sharer.php?u={{ url('r/'.$result->slug) }}', '{{ $result->slug }}')"><i class="fa fa-facebook-square"></i> @lang('app.quizz_share_fb')</a>
                            </div>
                            <div class="col-md-2" align="center">
                                <a class="btn btn-twitter btn-fill btn-lg btn-block btn-magnify" href="#" onclick="Share('https://twitter.com/intenq/tweet?original_referer={{ Request::url() }}&text={{ $result->quizz->translation->title }} {{ Request::url() }}', '{{ $result->slug }}')"><i class="fa fa-twitter-square"></i><span class="hidden-md hidden-lg"> @lang('app.quizz_share_twitter')</span></a>
                            </div>
                        </div>
                        @if($result->quizz->type == 'friend')
                            <a href="{{ url('q/'.$result->quizz->slug) }}" class="btn btn-lg btn-fill btn-primary btn-block btn-magnify"><i class="fa fa-users"></i> @lang('app.choose_friend')</a>
                        @else
                            <a href="{{ url('q/'.$result->quizz->slug) }}" class="btn btn-lg btn-fill btn-primary btn-block btn-move-right">@lang('app.do_quizz') <i class="ti-angle-right"></i></a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4 items-row hidden-xs hidden-sm">
                @foreach($quizzs->random(3) as $quizz)
                    <div class="col-md-12">
                        <div class="card card-hover-effect" data-background="color" data-color="nude-gray">
                            <a href="{{ url('q/'.$quizz->slug) }}">
                                <div class="header header-with-icon" style="background: url('{{ url('uploads/quizz/'.$quizz->image) }}') no-repeat center / cover">
                                </div>
                                <div class="content text-center">
                                    <h6 class="title"><a href="{{ url('q/'.$quizz->slug) }}">{{ $quizz->translation->title }}</a></h6>
                                </div>
                                <div class="card-footer" align="center">
                                    @if($quizz->type == 'friend')
                                        <a href="{{ url('q/'.$quizz->slug) }}" class="btn btn-icon btn-fill btn-primary btn-block btn-magnify"><i class="fa fa-users"></i> @lang('app.choose_friend')</a>
                                    @else
                                        <a href="{{ url('q/'.$quizz->slug) }}" class="btn btn-icon btn-fill btn-primary btn-block btn-move-right">@lang('app.do_quizz') <i class="ti-angle-right"></i></a>
                                    @endif
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="section section-gray">
        <div class="container">
            <div class="row items-row col-md-12">
                @foreach($quizzs as $quizz)
                    <div class="col-md-4 col-sm-6 item">
                        <div class="card card-hover-effect" data-background="color" data-color="nude-gray">
                            <a href="{{ url('q/'.$quizz->slug) }}">
                                <div class="header header-with-icon" style="background: url('{{ url('uploads/quizz/'.$quizz->image) }}') no-repeat center / cover">
                                </div>
                                <div class="content text-center">
                                    <h6 class="title"><a href="{{ url('q/'.$quizz->slug) }}">{{ $quizz->translation->title }}</a></h6>
                                </div>
                                <div class="card-footer" align="center">
                                    @if($quizz->type == 'friend')
                                        <a href="{{ url('q/'.$quizz->slug) }}" class="btn btn-icon btn-fill btn-primary btn-block btn-magnify"><i class="fa fa-users"></i> @lang('app.choose_friend')</a>
                                    @else
                                        <a href="{{ url('q/'.$quizz->slug) }}" class="btn btn-icon btn-fill btn-primary btn-block btn-move-right">@lang('app.do_quizz') <i class="ti-angle-right"></i></a>
                                    @endif
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        {{ $quizzs->render() }}
    </div>
@endsection

@section('js')
    <script src="{{ url('js/bootstrap3-typeahead.min.js') }}" type="texq/javascript"></script>
    <script src="{{ url('js/jquery.infinitescroll.min.js') }}" type="texq/javascript"></script>
@section('js')
    <script src="{{ url('js/bootstrap3-typeahead.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('js/jquery.infinitescroll.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        function Share(url, slug){
            var w = 680;
            var h = 320;
            var left = (screen.width / 2) - (w / 2);
            var top = 150;
            var win = window.open(url, 'Partager', 'toolbar=no, status=no, menubar=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
        }
        $(document).ready(function(){
            $(document).ready(function() {
                $('.items-row').infinitescroll({
                    loading: {
                        finishedMsg: "<br><div class='end-msg'>@lang('app.loading_no_tag')</div>",
                        msgText: '<div class="col-md-4 col-md-offset-4 text-center"><div class="preloader"><div class="uil-reload-css" style=""><div></div></div><h5>@lang('app.loading')</h5></div></div></div>',
                        img: "{{ url('img/blank.png') }}"
                    },
                    navSelector: "ul.pager",
                    nextSelector: "ul.pager a[rel='next']",
                    itemSelector: ".item",
                    extraScrollPx: 50,
                    bufferPx     : 40,
                    errorCallback: function(){},
                });
            });
        });
    </script>
@endsection
@endsection